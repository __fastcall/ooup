#ifndef __TASK4_PERCENTILE_HXX__
#define __TASK4_PERCENTILE_HXX__

#include <vector>

class AbstractPercentileFinder {
public:
    virtual double FindPercentile(double percentile, const int* numbers, int count) const = 0;
    double FindPercentile(double percentile, const std::vector<int>& numbers) const;
};

class NearestRankPercentileFinder : public AbstractPercentileFinder {
public:
    virtual double FindPercentile(double percentile, const int* numbers, int count) const override;
};

class ClosestRanksLerpPercentileFinder : public AbstractPercentileFinder {
public:
    virtual double FindPercentile(double percentile, const int* numbers, int count) const override;
};

#endif
