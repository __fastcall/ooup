#include "Task5_Numbers.hxx"
#include <iostream>
#include <thread>
#include <chrono>
#include <algorithm>
#include <ctime>
#include <cassert>

StreamIzvor::StreamIzvor(std::istream& stream) : m_Stream(stream) { }

int StreamIzvor::SljedeciBroj() {
    int broj;
    do {
        while (m_Stream.peek() == ' ' || m_Stream.peek() == '\t' || m_Stream.peek() == '\r' || m_Stream.peek() == '\n') m_Stream.get();
        if (m_Stream.eof()) return -1;
        m_Stream >> broj;
    } while (broj < 0);
    return broj;
}

TipkovnickiIzvor::TipkovnickiIzvor() : StreamIzvor(std::cin) { }

DatotecniIzvor::DatotecniIzvor(const std::string& imedatoteke) : m_Datoteka(imedatoteke), StreamIzvor(m_Datoteka) {
    assert(!m_Datoteka.bad());
}

SlijedBrojeva::SlijedBrojeva(ApstraktniIzvor& izvor) : m_Izvor(izvor) {
    RegistrirajAkciju("sd", "spremi datoteku", SlijedBrojeva::SpremiDatoteku);
    RegistrirajAkciju("is", "ispisi sumu", SlijedBrojeva::IspisiSumu);
    RegistrirajAkciju("ip", "ispisi prosjek", SlijedBrojeva::IspisiProsjek);
    RegistrirajAkciju("im", "ispisi medijan", SlijedBrojeva::IspisiMedijan);
}

void SlijedBrojeva::Kreni() {
    for (int broj = m_Izvor.SljedeciBroj(); broj != -1; broj = m_Izvor.SljedeciBroj()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "Ucitan broj " << broj << "." << std::endl;
        m_Brojevi.push_back(broj);
        while (true) {
            std::cout << "> ";
            std::string akcija;
            std::cin >> akcija;
            if (akcija == "d") {
                break;
            } else if (akcija == "p") {
                std::terminate();
            } else {
                if (m_Akcije.count(akcija) > 0) {
                    m_Akcije[akcija].second(m_Brojevi);
                } else {
                    std::cout << "Nepoznata akcija '" << akcija << "'" << std::endl;
                    std::cout << "Moguce akcije:" << std::endl;
                    std::cout << "d - dalje" << std::endl;
                    std::cout << "p - prekini" << std::endl;
                    for (const std::pair<std::string, std::pair<std::string, Akcija>>& par : m_Akcije) {
                        std::cout << par.first << " - " << par.second.first << std::endl;
                    }
                }
            }
        }
    }
    std::cout << "Kraj." << std::endl;
}

bool SlijedBrojeva::RegistrirajAkciju(const std::string& naredba, const std::string& opis, Akcija fja) {
    if (m_Akcije.count(naredba) > 0) return false;
    m_Akcije[naredba] = std::pair<std::string, Akcija>(opis, fja);
    return true;
}

bool SlijedBrojeva::DeregistrirajAkciju(const std::string& naredba) {
    return m_Akcije.erase(naredba);
}

void SlijedBrojeva::SpremiDatoteku(const std::vector<int>& brojevi) {
    std::string imedatoteke;
    std::cin >> imedatoteke;
    std::ofstream ofs(imedatoteke);
    for (int i : brojevi)
        ofs << i << std::endl;
    time_t t;
    std::time(&t);
    ofs << "DATUM/VRIJEME: " << std::ctime(&t) << std::endl;
}

void SlijedBrojeva::IspisiSumu(const std::vector<int>& brojevi) {
    int suma = 0;
    for (int i : brojevi)
        suma += i;
    std::cout << "Suma: " << suma << std::endl;
}

void SlijedBrojeva::IspisiProsjek(const std::vector<int>& brojevi) {
    int suma = 0;
    for (int i : brojevi)
        suma += i;
    std::cout << "Prosjek: " << (double)suma / brojevi.size() << std::endl;
}

void SlijedBrojeva::IspisiMedijan(const std::vector<int>& brojevi) {
    std::vector<int> sorted(brojevi);
    std::sort(sorted.begin(), sorted.end());
    double medijan;
    if (sorted.size() % 2 == 0)
        medijan = (sorted[sorted.size() / 2 - 1] + sorted[sorted.size() / 2]) / 2.0;
    else
        medijan = sorted[sorted.size() / 2];
    std::cout << "Medijan: " << medijan << std::endl;
}
