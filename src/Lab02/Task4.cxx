#include "Task4_Tester.hxx"
#include <iostream>

void prnnums(const std::vector<int>& vec) {
    std::cout << "N = [ ";
    for (int i : vec) std::cout << i << " ";
    std::cout << "]" << std::endl;
}

int main() {

    SequentialNumberGenerator sng(12, 20, 2);
    RandomNumberGenerator rng(5, 2.5, 1.1);
    FibonacciNumberGenerator fng(5);
    NearestRankPercentileFinder nrpf;
    ClosestRanksLerpPercentileFinder crlpf;

    DistributionTester dt(sng);
    std::cout << "Sequential + Nearest Rank:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(nrpf);
    std::cout << std::endl;

    std::cout << "Sequential + Closest Ranks Lerp:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(crlpf);
    std::cout << std::endl;

    dt.Regenerate(rng);
    std::cout << "Random + Nearest Rank:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(nrpf);
    std::cout << std::endl;

    std::cout << "Random + Closest Ranks Lerp:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(crlpf);
    std::cout << std::endl;

    dt.Regenerate(fng);
    std::cout << "Fibonacci + Nearest Rank:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(nrpf);
    std::cout << std::endl;

    std::cout << "Fibonacci + Closest Ranks Lerp:" << std::endl;
    prnnums(dt.GetNumbers());
    dt.Test(crlpf);
    std::cout << std::endl;

    return 0;

}
