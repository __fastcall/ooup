#ifndef __TASK5_NUMBERS_HXX__
#define __TASK5_NUMBERS_HXX__

#include <istream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>

class ApstraktniIzvor {
public:
    virtual int SljedeciBroj() = 0;
};

class StreamIzvor : public ApstraktniIzvor {
protected:
    StreamIzvor(std::istream& stream);
public:
    virtual int SljedeciBroj() override;
private:
    std::istream& m_Stream;
};

class TipkovnickiIzvor : public StreamIzvor {
public:
    TipkovnickiIzvor();
};

class DatotecniIzvor : public StreamIzvor {
public:
    DatotecniIzvor(const std::string& imedatoteke);
private:
    std::ifstream m_Datoteka;
};

typedef void (*Akcija)(const std::vector<int>& brojevi);

class SlijedBrojeva {
public:
    SlijedBrojeva(ApstraktniIzvor& izvor);
    void Kreni();
    bool RegistrirajAkciju(const std::string& naredba, const std::string& opis, Akcija fja);
    bool DeregistrirajAkciju(const std::string& naredba);
private:
    static void SpremiDatoteku(const std::vector<int>& brojevi);
    static void IspisiSumu(const std::vector<int>& brojevi);
    static void IspisiProsjek(const std::vector<int>& brojevi);
    static void IspisiMedijan(const std::vector<int>& brojevi);
    ApstraktniIzvor& m_Izvor;
    std::vector<int> m_Brojevi;
    std::unordered_map<std::string, std::pair<std::string, Akcija>> m_Akcije;
};

#endif
