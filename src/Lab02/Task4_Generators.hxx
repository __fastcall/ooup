#ifndef __TASK4_GENERATORS_HXX__
#define __TASK4_GENERATORS_HXX__

#include <vector>
#include <random>

class AbstractNumberGenerator {
public:
    virtual std::vector<int> Generate() = 0;
};

class SequentialNumberGenerator : public AbstractNumberGenerator {
public:
    SequentialNumberGenerator(int lowerBound, int upperBound, int increment = 1);
    virtual std::vector<int> Generate() override;
private:
    int m_LowerBound, m_UpperBound, m_Increment;
};

class RandomNumberGenerator : public AbstractNumberGenerator {
public:
    RandomNumberGenerator(int count, double meanValue = 0.0, double standardDeviation = 1.0);
    virtual std::vector<int> Generate() override;
private:
    int m_Count;
    std::normal_distribution<double> m_NormalDistribution;
    std::default_random_engine m_RandomEngine;
};

class FibonacciNumberGenerator : public AbstractNumberGenerator {
public:
    FibonacciNumberGenerator(int count);
    virtual std::vector<int> Generate() override;
private:
    int m_Count;
};

#endif
