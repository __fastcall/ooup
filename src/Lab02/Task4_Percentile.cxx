#include "Task4_Percentile.hxx"
#include <algorithm>
#include <cmath>
#include <cassert>

double AbstractPercentileFinder::FindPercentile(double percentile, const std::vector<int>& numbers) const {
    return FindPercentile(percentile, numbers.data(), numbers.size());
}

double NearestRankPercentileFinder::FindPercentile(double percentile, const int* numbers, int count) const {
    assert(percentile >= 0.0 && percentile <= 100.0);
    assert(numbers != NULL && count > 0);
    std::vector<int> sorted(numbers, numbers + count);
    std::sort(sorted.begin(), sorted.end());
    int index = static_cast<int>(std::ceil((percentile / 100.0) * count)) - 1;
    return static_cast<double>(sorted[index]);
}

double ClosestRanksLerpPercentileFinder::FindPercentile(double percentile, const int* numbers, int count) const {
    assert(percentile >= 0.0 && percentile <= 100.0);
    assert(numbers != NULL && count > 0);
    std::vector<int> sorted(numbers, numbers + count);
    std::sort(sorted.begin(), sorted.end());
    double limit = 50.0 / count;
    if (percentile <= limit)
        return static_cast<double>(sorted[0]);
    else if (percentile >= 100.0 - limit)
        return static_cast<double>(sorted[count - 1]);
    int index = static_cast<int>(std::floor((percentile - limit) / (2.0 * limit)));
    return sorted[index] + count * (percentile - limit * (1 + 2 * index)) * (sorted[index + 1] - sorted[index]) / 100.0;
}
