#include <stddef.h>
#include <stdio.h>
#include <string.h>

const void* mymax(const void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*)) {
    if (nmemb <= 0) return (const void*)NULL;
    const void* maxmemb = base;
    for (size_t i = 1; i < nmemb; ++i) {
        const void* memb = (const void*)((const char*)base + i * size);
        if (compar(memb, maxmemb)) maxmemb = memb; 
    }
    return maxmemb;
}

int gt_int(const void* a, const void* b) {
    return (*((const int*)a) > *((const int*)b));
}

int gt_char(const void* a, const void* b) {
    return (*((const char*)a) > *((const char*)b));
}

int gt_str(const void* a, const void* b) {
    const char* astr = *((const char**)a);
    const char* bstr = *((const char**)b);
    return (strcmp(astr, bstr) > 0);
}

int main() {
    int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
    char arr_char[] = "Suncana strana ulice";
    const char* arr_str[] = {
        "Gle", "malu", "vocku", "poslije", "kise",
        "Puna", "je", "kapi", "pa", "ih", "njise"
    };
    printf("mymax(arr_int) = %i\n", *((int*)mymax(arr_int, sizeof(arr_int) / sizeof(*arr_int), sizeof(*arr_int), gt_int)));
    printf("mymax(arr_char) = %c\n", *((char*)mymax(arr_char, strlen(arr_char), sizeof(*arr_char), gt_char)));
    printf("mymax(arr_str) = %s\n", *((char**)mymax(arr_str, sizeof(arr_str) / sizeof(*arr_str), sizeof(*arr_str), gt_str)));
    return 0;
}
