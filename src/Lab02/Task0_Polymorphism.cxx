#include <iostream>

struct Point {
    int x; int y;
};

struct Shape {
    public:
        virtual void draw() = 0;
        virtual void move(int dx, int dy) = 0;
};

struct Circle : public Shape {
    public:
        Circle(double radius = 1.0, Point center = { 0, 0 }) : radius_(radius), center_(center) { }
        virtual void draw() override { std::cerr << "Draw Circle" << std::endl; }
        virtual void move(int dx, int dy) override { std::cerr << "Move Circle" << std::endl; center_.x += dx; center_.y += dy; }
    private:
        double radius_;
        Point center_;
};

struct Square : public Shape {
    public:
        Square(double side = 1.0, Point center = { 0, 0 }) : side_(side), center_(center) { }
        virtual void draw() override { std::cerr << "Draw Square" << std::endl; }
        virtual void move(int dx, int dy) override { std::cerr << "Move Square" << std::endl; center_.x += dx; center_.y += dy; }
    private:
        double side_;
        Point center_;
};

struct Rhomb : public Shape {
    public:
        Rhomb(double side = 1.0, double angle = 45.0, Point center = { 0, 0 }) : side_(side), angle_(angle), center_(center) { }
        virtual void draw() override { std::cerr << "Draw Rhomb" << std::endl; }
        virtual void move(int dx, int dy) override { std::cerr << "Move Rhomb" << std::endl; center_.x += dx; center_.y += dy; }
    private:
        double side_;
        double angle_;
        Point center_;
};

void drawShapes(Shape** shapes, int n) {
    for (int i = 0; i < n; ++i) {
        Shape* s = shapes[i];
        s->draw();
    }
}

void moveShapes(Shape** shapes, int n, int dx, int dy) {
    for (int i = 0; i < n; ++i) {
        Shape* s = shapes[i];
        s->move(dx, dy);
    }
}

int main() {
    Shape* shapes[5];
    shapes[0] = new Circle;
    shapes[1] = new Square;
    shapes[2] = new Square;
    shapes[3] = new Circle;
    shapes[4] = new Rhomb;
    drawShapes(shapes, 5);
    moveShapes(shapes, 5, 100, 100);
    return 0;
}
