#include "Task4_Generators.hxx"
#include <chrono>
#include <cassert>

SequentialNumberGenerator::SequentialNumberGenerator(int lowerBound, int upperBound, int increment) : m_LowerBound(lowerBound), m_UpperBound(upperBound), m_Increment(increment) {
    assert(upperBound >= lowerBound);
}

std::vector<int> SequentialNumberGenerator::Generate() {
    std::vector<int> result;
    result.reserve((m_UpperBound - m_LowerBound) / m_Increment + 1);
    for (int i = m_LowerBound; i <= m_UpperBound; i += m_Increment)
        result.push_back(i);
    return result;
}

RandomNumberGenerator::RandomNumberGenerator(int count, double meanValue, double standardDeviation) : m_Count(count), m_NormalDistribution(meanValue, standardDeviation), m_RandomEngine(std::chrono::system_clock::now().time_since_epoch().count()) {
    assert(standardDeviation >= 0.0);
    assert(count > 0);
}

std::vector<int> RandomNumberGenerator::Generate() {
    std::vector<int> result;
    result.reserve(m_Count);
    for (int i = 0; i < m_Count; ++i)
        result.push_back(static_cast<int>(m_NormalDistribution(m_RandomEngine)));
    return result;
}

FibonacciNumberGenerator::FibonacciNumberGenerator(int count) : m_Count(count) {
    assert(count > 0);
}

std::vector<int> FibonacciNumberGenerator::Generate() {
    std::vector<int> result;
    result.reserve(m_Count);
    result.push_back(0);
    int prev = 0, curr = 1;
    for (int i = 1; i < m_Count; ++i) {
        result.push_back(curr);
        int pb = prev;
        prev = curr;
        curr += pb;
    }
    return result;
}
