def mymax(iterable, key = lambda x: x):
    max_x = max_key = None
    for x in iterable:
        x_key = key(x)
        if ((max_key == None) or (x_key > max_key)):
            max_x = x
            max_key = x_key
    return max_x

def main():
    # List test
    ints = [1, 3, 5, 7, 4, 6, 9, 2, 0]
    chars = "Suncana strana ulice"
    strings = ["Gle", "malu", "vocku", "poslije", "kise", "Puna", "je", "kapi", "pa", "ih", "njise"]
    print("maxint = " + str(mymax(ints)))
    print("maxchar = " + str(mymax(chars)))
    print("maxstring = " + mymax(strings))
    print("maxstringlen = " + mymax(strings, lambda x: len(x)))
    # Dict test
    D = {'burek': 8, 'buhtla': 5}
    print("maxD = " + mymax(D, lambda x: D.get(x)))
    # People test
    P = {("Ante", "Matic"), ("Ante", "Antic"), ("Ante", "Zvonic"), ("Ante", "Ivic")}
    maxP = mymax(P)
    print("maxP = " + maxP[0] + " " + maxP[1])

if (__name__ == '__main__'):
    main()
