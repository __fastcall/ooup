#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <list>
#include <set>

template<typename T>
bool gt(T a, T b) {
    return *a > *b;
}

template<>
bool gt(const char** a, const char** b) {
    return (std::strcmp(*a, *b) > 0);
}

template<typename T> /* T is iterator */
T mymax(T first, T last, bool (*pred)(T, T) = gt<T>) {
    T max = first++;
    while (first != last) {
        if (pred(first, max)) max = first;
        first++;
    }
    return max;
}

int main() {
    int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
    char arr_char[] = "Suncana strana ulice";
    const char* arr_str[] = {
        "Gle", "malu", "vocku", "poslije", "kise",
        "Puna", "je", "kapi", "pa", "ih", "njise"
    };
    std::vector<int> vec_int = { 1, 5, 2, 12, 5, -6, 4, 13, -24 };
    std::set<std::string> set_str = { "Beta", "Epsilon", "Gama", "Alfa", "Delta" };
    std::list<unsigned int> list_uint = { 0, 1, 2, 3, 4, 5, 6, 7 };
    std::cout << "mymax(arr_int) = " << *mymax(&arr_int[0], &arr_int[sizeof(arr_int) / sizeof(*arr_int)]) << std::endl;
    std::cout << "mymax(arr_char) = " << *mymax(&arr_char[0], &arr_char[sizeof(arr_char) / sizeof(*arr_char)]) << std::endl;
    std::cout << "mymax(arr_str) = " << *mymax(&arr_str[0], &arr_str[sizeof(arr_str) / sizeof(*arr_str)]) << std::endl;
    std::cout << "mymax(vec_int) = " << *mymax(vec_int.begin(), vec_int.end()) << std::endl;
    std::cout << "mymax(set_str) = " << *mymax(set_str.begin(), set_str.end()) << std::endl;
    std::cout << "mymax(list_uint) = " << *mymax(list_uint.begin(), list_uint.end()) << std::endl;
    return 0;
}
