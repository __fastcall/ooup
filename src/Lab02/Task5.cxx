#include "Task5_Numbers.hxx"
#include <iostream>
#include <vector>

void IspisiBrojeve(const std::vector<int>& brojevi) {
    std::cout << "[ ";
    for (int i : brojevi)
        std::cout << i << " ";
    std::cout << "]" << std::endl;
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        TipkovnickiIzvor ti;
        SlijedBrojeva sb(ti);
        sb.RegistrirajAkciju("ib", "ispisi brojeve", IspisiBrojeve);
        sb.Kreni();
    } else if (argc == 2) {
        std::string ime = argv[1];
        DatotecniIzvor di = DatotecniIzvor(ime);
        SlijedBrojeva sb(di);
        sb.RegistrirajAkciju("ib", "ispisi brojeve", IspisiBrojeve);
        sb.Kreni();
    } else {
        std::cerr << "POGRESKA: Nevaljani argumenti." << std::endl;
        return -1;
    }
    return 0;
}
