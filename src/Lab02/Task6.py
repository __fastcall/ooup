import ast, operator, re

# Binary operators for arithmetic evaluation
binOps = {
    ast.Add: operator.add,
    ast.Sub: operator.sub,
    ast.Mult: operator.mul,
    ast.Div: operator.div,
    ast.Mod: operator.mod
}

# Arithmetic evaluation
def arithmeticEval(s):
    node = ast.parse(s, mode = "eval")
    def _eval(node):
        if isinstance(node, ast.Expression):
            return _eval(node.body)
        elif isinstance(node, ast.Str):
            return node.s
        elif isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.BinOp):
            return binOps[type(node.op)](_eval(node.left), _eval(node.right))
        else:
            raise Exception("Unsupported type: %s" % format(node))
    return _eval(node.body)

class Cell:
    # Cell constructor
    def __init__(self, exp):
        self.exp = exp
        self.value = None
        self.refs = []
        self.refby = []
        pass
    # Invalidates cell (and all cells that depend on this cell)
    def invalidate(self):
        self.value = None
        for ref in self.refby:
            ref.invalidate()
        pass

class Sheet:
    # Sheet constructor
    def __init__(self, width = 8, height = 8):
        self.width = width
        self.height = height
        self.cells = []
        for _ in range(width):
            l = []
            for _ in range(height):
                l.append(Cell("0"))
            self.cells.append(l)
        pass
    # Get cell position from reference, example: sheet.__cellpos__("B1") returns (2, 1)
    def __cellpos__(self, ref):
        match = re.search("([a-zA-Z]+)([1-9][0-9]*)", ref)
        if (match == None):
            raise Exception("Cell format not valid.")
        column = 0
        for c in match.group(1):
            column = column * (ord("z") - ord("a") + 1) + ord(c.lower()) - ord("a") + 1
        row = int(match.group(2))
        return (column, row)
    # Get cell from reference, example: sheet.cell("B1") returns cell in 2nd column and 1st row
    def cell(self, ref):
        cellpos = self.__cellpos__(ref)
        cellpos = (cellpos[0] - 1, cellpos[1] - 1)
        if (cellpos[0] < 0 or cellpos[0] >= self.width or cellpos[1] < 0 or cellpos[1] >= self.height):
            raise Exception("Cell out of bounds.")
        return self.cells[cellpos[0]][cellpos[1]]
    # Detects if there is a cycle dependency with newrefs
    def __detectcycle__(self, cell, newrefs):
        opened = newrefs[:]
        closed = []
        while (len(opened) > 0):
            head = opened.pop(0)
            closed.append(head)
            if (head == cell):
                return True
            for ref in self.getrefs(head):
                if (ref not in closed):
                    opened.append(ref)
        return False
    # Set cell expression of cell reference, example: sheet.set("B1", "2") sets expression to "2" of cell in 2nd column and 1st row
    def set(self, ref, content):
        cell = self.cell(ref)
        newrefs = self.__findrefs__(cell, content)
        if self.__detectcycle__(cell, newrefs):
            raise Exception("Cycle detected.")
        for ref in self.getrefs(cell):
            ref.refby.remove(cell)
        cell.refs = newrefs
        for ref in self.getrefs(cell):
            ref.refby.append(cell)
        cell.exp = content
        cell.invalidate()
        pass
    # Finds list of cells that cell references in its expression
    def __findrefs__(self, cell, exp):
        refs = []
        matches = re.findall("([a-zA-Z]+[1-9][0-9]*)", exp)
        for match in matches:
            refs.append(self.cell(match))
        return refs
    # Gets list of cells that cell references in its expression
    def getrefs(self, cell):
        return cell.refs
    # Evaluates cell expression and returns the value (returns cached if still valid)
    def evaluate(self, cell):
        if (cell.value != None):
            return cell.value
        exp = cell.exp
        while True:
            matches = re.findall("([a-zA-Z]+[1-9][0-9]*)", exp)
            if (len(matches) == 0):
                break
            for match in matches:
                exp = str.replace(exp, match, str(self.evaluate(self.cell(match))))
        cell.value = arithmeticEval(exp)
        return cell.value
    # Returns column name string from column index, example: index of 2 will return "B"
    def __columnstr__(self, index):
        if (index > ord("z") - ord("a") + 1):
            return self.__columnstr__((index - 1) / (ord("z") - ord("a") + 1)) + self.__columnstr__((index - 1) % (ord("z") - ord("a") + 1) + 1)
        return chr(ord("A") + index - 1)
    # Returns string of the table
    def __str__(self):
        s = "    "
        for i in range(self.width):
            s += self.__columnstr__(i + 1).rjust(3) + " "
        s += "\n"
        for j in range(self.height):
            s += str(j + 1).rjust(3) + " "
            for i in range(self.width):
                s += str(self.evaluate(self.cells[i][j])).rjust(3) + " "
            s += "\n"
        return s

def main():
    s = Sheet(5, 5)
    print(s)

    s.set("A1", "2")
    s.set("A2", "5")
    s.set("A3", "A1+A2")
    print(s)

    s.set("A1", "4")
    s.set("A4", "A1+A3")
    print(s)

    try:
        s.set("A1", "A3")
    except Exception as e:
        print("Caught exception: %s" % e)
    print(s)

if __name__ == "__main__":
    main()
