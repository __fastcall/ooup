#include "Task4_Tester.hxx"
#include <iostream>

DistributionTester::DistributionTester(AbstractNumberGenerator& ng) {
    Regenerate(ng);
}

void DistributionTester::Regenerate(AbstractNumberGenerator& ng) {
    m_Numbers = ng.Generate();
}

void DistributionTester::Test(const AbstractPercentileFinder& pf) const {
    for (int p = 10; p < 100; p += 10)
        std::cout << "p(" << p << ") = " << pf.FindPercentile(p, m_Numbers) << std::endl;
}
