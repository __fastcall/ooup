#ifndef __TASK4_TESTER_HXX__
#define __TASK4_TESTER_HXX__

#include "Task4_Generators.hxx"
#include "Task4_Percentile.hxx"
#include <vector>

class DistributionTester {
public:
    DistributionTester(AbstractNumberGenerator& ng);
    void Regenerate(AbstractNumberGenerator& ng);
    void Test(const AbstractPercentileFinder& pf) const;
    inline const std::vector<int>& GetNumbers() const { return m_Numbers; }
private:
    std::vector<int> m_Numbers;
};

#endif
