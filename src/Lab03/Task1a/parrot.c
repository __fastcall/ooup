#include "animal.h"

/* Parrot "class". */
typedef struct {
    Animal base;
} Parrot;

/* Parrot type. */
extern const char* parrot_type();

/* Parrot size. */
extern size_t parrot_size();

/* Parrot constructor. */
extern void parrot_ctor(Parrot* obj, const char* name);

/* Parrot destructor (vtable 0). */
extern void parrot_dtor(Parrot* obj);

/* Parrot name (vtable 1). */
/* extern const char* parrot_name(Parrot* obj); */

/* Parrot greet (vtable 2). */
extern const char* parrot_greet();

/* Parrot menu (vtable 3). */
extern const char* parrot_menu();

/* Parrot virtual table. */
static FPTR parrot_vtable[] = {
    parrot_dtor,
    animal_name,
    parrot_greet,
    parrot_menu
};

/* Parrot type. */
const char* parrot_type() {

    /* Return animal string identifier. */
    return "animal";

}

/* Parrot size. */
size_t parrot_size() {

    /* Return parrot structure size. */
    return sizeof(Parrot);

}

/* Parrot constructor. */
void parrot_ctor(Parrot* obj, const char* name) {

    /* Call base constructor(s). */
    animal_ctor((Animal*)obj, name);

    /* Set virtual table pointer. */
    obj->base.vtable = parrot_vtable;

    /* Constructor body. */
    /* ... */

}

/* Parrot destructor (vtable 0). */
void parrot_dtor(Parrot* obj) {

    /* Set virtual table pointer. */
    obj->base.vtable = parrot_vtable;

    /* Destructor body. */
    /* ... */

    /* Call base destructor(s). */
    animal_dtor((Animal*)obj);

}

/* Parrot greet (vtable 2). */
const char* parrot_greet() {

    /* Return parrot greeting. */
    return "Sto mu gromova!";

}

/* Parrot menu (vtable 3). */
const char* parrot_menu() {

    /* Return parrot menu. */
    return "brazilske orahe";

}
