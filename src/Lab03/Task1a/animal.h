#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include <sys/types.h>

/* Virtual function call macros. */
#define ANIMAL_VCALL(o, i, t, ...) (((t(*)())(((Animal*)o)->vtable[i]))(__VA_ARGS__))
#define ANIMAL_DTOR(o) ANIMAL_VCALL(o, 0, void, o)
#define ANIMAL_NAME(o) ANIMAL_VCALL(o, 1, const char*, o)
#define ANIMAL_GREET(o) ANIMAL_VCALL(o, 2, const char*)
#define ANIMAL_MENU(o) ANIMAL_VCALL(o, 3, const char*)

/* Generic function pointer type. */
typedef void* FPTR;

/* Animal base "class". */
typedef struct {
    FPTR* vtable;
    char* name;
} Animal;

/* Animal type. */
extern const char* animal_type();

/* Animal size. */
extern size_t animal_size();

/* Animal constructor. */
extern void animal_ctor(Animal* obj, const char* name);

/* Animal destructor (vtable 0). */
extern void animal_dtor(Animal* obj);

/* Animal name (vtable 1). */
extern const char* animal_name(Animal* obj);

/* Animal greet (vtable 2). */
/* extern const char* animal_greet(); */

/* Animal menu (vtable 3). */
/* extern const char* animal_menu(); */

#endif
