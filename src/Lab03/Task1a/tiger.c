#include "animal.h"

/* Tiger "class". */
typedef struct {
    Animal base;
} Tiger;

/* Tiger type. */
extern const char* tiger_type();

/* Tiger size. */
extern size_t tiger_size();

/* Tiger constructor. */
extern void tiger_ctor(Tiger* obj, const char* name);

/* Tiger destructor (vtable 0). */
extern void tiger_dtor(Tiger* obj);

/* Tiger name (vtable 1). */
/* extern const char* tiger_name(Tiger* obj); */

/* Tiger greet (vtable 2). */
extern const char* tiger_greet();

/* Tiger menu (vtable 3). */
extern const char* tiger_menu();

/* Tiger virtual table. */
static FPTR tiger_vtable[] = {
    tiger_dtor,
    animal_name,
    tiger_greet,
    tiger_menu
};

/* Tiger type. */
const char* tiger_type() {

    /* Return animal string identifier. */
    return "animal";

}

/* Tiger size. */
size_t tiger_size() {

    /* Return tiger structure size. */
    return sizeof(Tiger);

}

/* Tiger constructor. */
void tiger_ctor(Tiger* obj, const char* name) {

    /* Call base constructor(s). */
    animal_ctor((Animal*)obj, name);

    /* Set virtual table pointer. */
    obj->base.vtable = tiger_vtable;

    /* Constructor body. */
    /* ... */

}

/* Tiger destructor (vtable 0). */
void tiger_dtor(Tiger* obj) {

    /* Set virtual table pointer. */
    obj->base.vtable = tiger_vtable;

    /* Destructor body. */
    /* ... */

    /* Call base destructor(s). */
    animal_dtor((Animal*)obj);

}

/* Tiger greet (vtable 2). */
const char* tiger_greet() {

    /* Return tiger greeting. */
    return "Mijau!";

}

/* Tiger menu (vtable 3). */
const char* tiger_menu() {

    /* Return tiger menu. */
    return "mlako mlijeko";

}
