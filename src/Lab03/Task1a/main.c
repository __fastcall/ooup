#include "animal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <dlfcn.h>
#include <errno.h>
#include <err.h>

/* Animal factory creator. */
Animal* animal_factory_create(const char* type, const char* name) {

    /* Reserve buffer. */
    size_t len = strlen(type);
    char buff[len + 11];

    /* Assemble library file name string. */
    memcpy(buff, "./lib", 5);
    memcpy(buff + 5, type, len);
    memcpy(buff + len + 5, ".so", 4);

    /* Open dynamic library. */
    void* so = dlopen(buff, RTLD_NOW);
    if (so == NULL) {
        warnx("%s", dlerror());
        return NULL;
    }

    /* Assemble type function name string. */
    memcpy(buff + len + 5, "_type", 6);
    
    /* Get type function. */
    const char* (*ftype)() = dlsym(so, buff + 5);
    if (ftype == NULL) {
        warnx("%s", dlerror());
        dlclose(so);
        return NULL;
    }

    /* Assure that the type string is supported. */
    const char* typestr = ftype();
    if (strcmp(typestr, "animal") != 0) {
        warnx("type %s cannot be instantiated as animal", typestr);
        dlclose(so);
        return NULL;
    }

    /* Assemble size function name string. */
    memcpy(buff + len + 5, "_size", 5);
    
    /* Get size function. */
    size_t (*fsize)() = dlsym(so, buff + 5);
    if (fsize == NULL) {
        warnx("%s", dlerror());
        dlclose(so);
        return NULL;
    }

    /* Assemble constructor function name string. */
    memcpy(buff + len + 5, "_ctor", 5);
    
    /* Get constructor function. */
    void (*fctor)() = dlsym(so, buff + 5);
    if (fctor == NULL) {
        warnx("%s", dlerror());
        dlclose(so);
        return NULL;
    }

    /* Allocate animal memory. */
    Animal* animal = (Animal*)malloc(fsize());
    if (animal == NULL) {
        warnx("%s", strerror(errno));
        dlclose(so);
        return NULL;
    }

    /* Call animal constructor. */
    fctor(animal, name);

    /* Return animal instance. */
    return animal;

}

/* Animal factory destroyer. */
void animal_factory_destroy(Animal* animal) {

    /* Call virtual destructor. */
    ANIMAL_DTOR(animal);

    /* Free memory. */
    free(animal);

}

/* Entry function. */
int main() {

    /* Create animals. */
    Animal* parrot = animal_factory_create("parrot", "Papiga"); assert(parrot != NULL);
    Animal* tiger = animal_factory_create("tiger", "Tigar"); assert(tiger != NULL);

    /* Print greetings. */
    printf("%s se glasa \"%s\"\n", ANIMAL_NAME(parrot), ANIMAL_GREET(parrot));
    printf("%s se glasa \"%s\"\n", ANIMAL_NAME(tiger), ANIMAL_GREET(tiger));

    /* Print menus. */
    printf("%s voli %s\n", ANIMAL_NAME(parrot), ANIMAL_MENU(parrot));
    printf("%s voli %s\n", ANIMAL_NAME(tiger), ANIMAL_MENU(tiger));

    /* Destroy animals. */
    animal_factory_destroy(parrot);
    animal_factory_destroy(tiger);

    /* Exit success. */
    return 0;

}
