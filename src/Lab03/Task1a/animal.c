#include "animal.h"
#include <stdlib.h>
#include <string.h>

/* Animal virtual table. */
static FPTR animal_vtable[] = {
    animal_dtor,
    animal_name,
    NULL,
    NULL
};

/* Animal type. */
const char* animal_type() {

    /* Return abstract animal string identifier. */
    return "abstract_animal";

}

/* Animal size. */
size_t animal_size() {

    /* Return animal structure size. */
    return sizeof(Animal);

}

/* Animal constructor. */
void animal_ctor(Animal* obj, const char* name) {

    /* Call base constructor(s). */
    /* ... */

    /* Set virtual table pointer. */
    obj->vtable = animal_vtable;

    /* Constructor body. */
    size_t len = strlen(name);
    obj->name = (char*)malloc(len + 1);
    memcpy(obj->name, name, len + 1);

}

/* Animal destructor (vtable 0). */
void animal_dtor(Animal* obj) {

    /* Set virtual table pointer. */
    obj->vtable = animal_vtable;

    /* Destructor body. */
    free(obj->name);

    /* Call base destructor(s). */
    /* ... */

}

/* Animal name (vtable 1). */
const char* animal_name(Animal* obj) {

    /* Return animal name. */
    return obj->name;

}
