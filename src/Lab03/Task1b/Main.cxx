#include "AnimalFactory.hxx"
#include <iostream>
#include <cassert>

/* Entry function. */
int main() {

    /* Create animals. */
    Animal* parrot = AnimalFactory::Create("parrot", "Papiga"); assert(parrot != nullptr);
    Animal* tiger = AnimalFactory::Create("tiger", "Tigar"); assert(tiger != nullptr);

    /* Print greetings. */
    std::cout << parrot->GetName() << " se glasa \"" << parrot->GetGreeting() << "\"" << std::endl;
    std::cout << tiger->GetName() << " se glasa \"" << tiger->GetGreeting() << "\"" << std::endl;

    /* Print menus. */
    std::cout << parrot->GetName() << " voli " << parrot->GetMenu() << std::endl;
    std::cout << tiger->GetName() << " voli " << tiger->GetMenu() << std::endl;

    /* Destroy animals. */
    AnimalFactory::Destroy(parrot);
    AnimalFactory::Destroy(tiger);

    /* Exit success. */
    return 0;

}
