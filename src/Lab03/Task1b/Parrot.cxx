#include "AnimalFactory.hxx"

/* Parrot class. */
class Parrot : public Animal {
public:
    Parrot(const std::string& name) : Animal(name) { }
    virtual ~Parrot() override { }
    virtual const std::string& GetGreeting() const override { return Parrot::Greeting; }
    virtual const std::string& GetMenu() const override { return Parrot::Menu; }
private:
    static Animal* Creator(const std::string& name) { return new Parrot(name); }
    static bool CreatorRegistered;
    static std::string Greeting, Menu;
};

/* Register parrot creator. */
bool Parrot::CreatorRegistered = AnimalFactory::RegisterCreator("parrot", Parrot::Creator);

/* Parrot greeting and menu. */
std::string Parrot::Greeting = "Sto mu gromova!";
std::string Parrot::Menu = "brazilske orahe";
