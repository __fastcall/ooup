#include "AnimalFactory.hxx"

/* Animal factory creator. */
Animal* AnimalFactory::Create(const std::string& type, const std::string& name) {
    
    /* Return null pointer if creator type is not registered. */
    if (AnimalFactory::Creators.count(type) == 0) return nullptr;

    /* Return new animal instance pointer. */
    return AnimalFactory::Creators[type](name);

}

/* Animal factory destroyer. */
void AnimalFactory::Destroy(Animal* obj) {

    /* Delete animal instance. */
    delete obj;

}

/* Register animal type creator. */
bool AnimalFactory::RegisterCreator(const std::string& type, AnimalCreator creator) {

    /* Return false if creator type is already registered. */
    if (AnimalFactory::Creators.count(type) > 0) return false;

    /* Add creator to unordered map. */
    AnimalFactory::Creators[type] = creator;
    return true;

}

/* Unregister animal type creator. */
bool AnimalFactory::UnregisterCreator(const std::string& type) {

    /* Remove creator from unordered map. */
    return (AnimalFactory::Creators.erase(type) > 0);

}

/* Animal creators. */
std::unordered_map<std::string, AnimalCreator> AnimalFactory::Creators;
