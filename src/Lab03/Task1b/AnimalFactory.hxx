#ifndef __ANIMAL_FACTORY_HXX__
#define __ANIMAL_FACTORY_HXX__

#include "Animal.hxx"
#include <unordered_map>

/* Animal creator function type. */
typedef Animal* (*AnimalCreator)(const std::string& name);

/* Animal factory. */
class AnimalFactory {
public:
    static Animal* Create(const std::string& type, const std::string& name);
    static void Destroy(Animal* obj);
    static bool RegisterCreator(const std::string& type, AnimalCreator creator);
    static bool UnregisterCreator(const std::string& type);
private:
    AnimalFactory() = delete;
    static std::unordered_map<std::string, AnimalCreator> Creators;
};

#endif
