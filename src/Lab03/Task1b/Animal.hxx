#ifndef __ANIMAL_HXX__
#define __ANIMAL_HXX__

#include <string>

/* Animal base class. */
class Animal {
public:
    Animal(const std::string& name);
    virtual ~Animal();
    virtual const std::string& GetName() const;
    virtual const std::string& GetGreeting() const = 0;
    virtual const std::string& GetMenu() const = 0;
private:
    std::string m_Name;
};

#endif
