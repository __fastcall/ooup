#include "Animal.hxx"

/* Animal constructor. */
Animal::Animal(const std::string& name) : m_Name(name) { }

/* Animal destructor. */
Animal::~Animal() { }

/* Get animal name. */
const std::string& Animal::GetName() const { return m_Name; }
