#include "AnimalFactory.hxx"

/* Tiger class. */
class Tiger : public Animal {
public:
    Tiger(const std::string& name) : Animal(name) { }
    virtual ~Tiger() override { }
    virtual const std::string& GetGreeting() const override { return Tiger::Greeting; }
    virtual const std::string& GetMenu() const override { return Tiger::Menu; }
private:
    static Animal* Creator(const std::string& name) { return new Tiger(name); }
    static bool CreatorRegistered;
    static std::string Greeting, Menu;
};

/* Register tiger creator. */
bool Tiger::CreatorRegistered = AnimalFactory::RegisterCreator("tiger", Tiger::Creator);

/* Tiger greeting and menu. */
std::string Tiger::Greeting = "Mijau!";
std::string Tiger::Menu = "mlako mlijeko";
