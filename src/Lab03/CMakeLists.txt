# Build task 1a only on Unix-like systems
if(UNIX)

    # Add task 1a subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Task1a)

endif()

# Add task 1b subdirectory
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Task1b)
