#include <stdio.h>
#include <stdlib.h>
#include "Task1_Animal.h"

static void animalPrintGreeting(struct Animal* animal) {
    printf("%s pozdravlja: %s\n", animal->name, ANIMAL_GREET(animal));
}

static void animalPrintMenu(struct Animal* animal) {
    printf("%s voli %s\n", animal->name, ANIMAL_MENU(animal));
}

static struct Animal* createDog(const char* name) {
    struct Dog* dog = (struct Dog*)malloc(sizeof(struct Dog));
    constructDog(dog, name);
    return (struct Animal*)dog;
}

static struct Animal* createCat(const char* name) {
    struct Cat* cat = (struct Cat*)malloc(sizeof(struct Cat));
    constructCat(cat, name);
    return (struct Animal*)cat;
}

static struct Dog* createDogs(int n) {
    struct Dog* dogs = (struct Dog*)malloc(n * sizeof(struct Dog));
    for (int i = 0; i < n; ++i)
        constructDog(dogs + i, "Pas");
    return dogs;
}

/* Tests animal polymorphism. */
static void testAnimals(void) {

    /* Create animal instances. */
    struct Animal* p1 = createDog("Hamlet");
    struct Animal* p2 = createCat("Ofelija");
    struct Animal* p3 = createDog("Polonije");

    /* Print greetings. */
    animalPrintGreeting(p1);
    animalPrintGreeting(p2);
    animalPrintGreeting(p3);

    /* Print menus. */
    animalPrintMenu(p1);
    animalPrintMenu(p2);
    animalPrintMenu(p3);

    /* Free allocated memory. */
    free(p1); free(p2); free(p3);

}

/* Entry method. */
int main(int argc, char** argv) {

    /* Test animals. */
    testAnimals();

    /* Stack test. */
    struct Dog dog;
    constructDog(&dog, "Stog");
    printf("Pas na stogu se glasa: %s\n", ANIMAL_GREET(&dog));

    /* Heap test. */
    struct Cat* cat = (struct Cat*)malloc(sizeof(struct Cat));
    constructCat(cat, "Gomila");
    printf("Macka na gomili se glasa: %s\n", ANIMAL_GREET(cat));
    free(cat);

    /* Create 10 dogs at once. */
    struct Dog* dogs = createDogs(10);
    for (int i = 0; i < 10; ++i)
        printf("Pas %2i od 10 se glasa: %s\n", i + 1, ANIMAL_GREET(dogs + i));

    return 0;

}
