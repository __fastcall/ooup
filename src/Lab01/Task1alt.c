#include <stdlib.h>
#include <stdio.h>
#include "Task1alt_Animal.h"

static struct Dog* createDog(const char* name) {
    struct Dog* dog = (struct Dog*)malloc(sizeof(struct Dog));
    D_Constructor(dog, name);
    return dog;
}

static struct Cat* createCat(const char* name) {
    struct Cat* cat = (struct Cat*)malloc(sizeof(struct Cat));
    C_Constructor(cat, name);
    return cat;
}

static void deleteAnimal(struct Animal** animal) {
    A_DESTRUCTOR(*animal);
    free(*animal);
    animal = NULL;
}

static struct Dog* createDogs(int n, const char* name) {
    int* mem = (int*)malloc(sizeof(int) + n * sizeof(struct Dog));
    *mem = n;
    struct Dog* dogs = (struct Dog*)(mem + 1);
    for (int i = 0; i < n; ++i)
        D_Constructor(dogs + i, name);
    return dogs;
}

static void deleteDogs(struct Dog** dogs) {
    int* mem = ((int*)*dogs) - 1;
    for (int i = 0; i < *mem; ++i)
        _D_Destructor((*dogs) + i); /* can be called directly because we know the type */
    free(mem);
    dogs = NULL;
}

static void animalPrintGreeting(const struct Animal* animal) {
    printf("%s pozdravlja: %s\n", animal->name, A_GREET(animal));
}

static void animalPrintMenu(const struct Animal* animal) {
    printf("%s voli %s\n", animal->name, A_MENU(animal));
}

static void testAnimals(void) {

    struct Animal* p1 = (struct Animal*)createDog("Hamlet");
    struct Animal* p2 = (struct Animal*)createCat("Ofelija");
    struct Animal* p3 = (struct Animal*)createDog("Polonije");

    animalPrintGreeting(p1);
    animalPrintGreeting(p2);
    animalPrintGreeting(p3);

    animalPrintMenu(p1);
    animalPrintMenu(p2);
    animalPrintMenu(p3);

    deleteAnimal(&p3);
    deleteAnimal(&p2);
    deleteAnimal(&p1);

}

static void testHeapStack() {
    
    /* Stack dog. */
    struct Dog dog;
    D_Constructor(&dog, "pas");
    printf("Na stogu je %s koji se glasa: %s\n", dog._base.name, _D_Greet(&dog)); /* can be called directly because we know the type */

    /* Heap cat. */
    struct Animal* cat = (struct Animal*)malloc(sizeof(struct Cat));
    C_Constructor((struct Cat*)cat, "macka");
    printf("Na gomili je %s koja se glasa: %s\n", cat->name, A_GREET(cat));

    /* Destructors. */
    A_DESTRUCTOR(cat);
    free(cat);
    _D_Destructor(&dog); /* can be called directly because we know the type */

}

static void testMassConstruction() {

    struct Dog* dogs = createDogs(10, "pas");

    for (int i = 0; i < 10; ++i)
        printf("%2d. %s se glasa: %s\n", i + 1, dogs[i]._base.name, A_GREET(dogs + i));

    deleteDogs(&dogs);

}

int main(int argc, char** argv) {

    testAnimals();

    testHeapStack();

    testMassConstruction();

    return 0;

}
