#ifndef __TASK2_FUNCTIONS_H__
#define __TASK2_FUNCTIONS_H__

#define _UF_VCALL(o, i, t, ...) (((t)((struct UnaryFunction*)o)->_vtable[i])(o, ##__VA_ARGS__))
#define UF_VALUE_AT(o, x) (_UF_VCALL(o, 0, DOUBLE_FPTR, (double)x))
#define UF_NEGATIVE_VALUE_AT(o, x) (_UF_VCALL(o, 1, DOUBLE_FPTR, (double)x))

/* Virtual function types. */
typedef void* FPTR;
typedef double (*DOUBLE_FPTR)();

/* Unary function "class". */
struct UnaryFunction {
    const FPTR* const _vtable;
    int lower_bound, upper_bound;
};

/* Square "class". */
struct Square {
    struct UnaryFunction _base;
};

/* Linear "class". */
struct Linear {
    struct UnaryFunction _base;
    double a, b;
};

/**
 * @brief Initializes an unary function instance.
 * @note Should only be called within constructors of derived classes.
 * @param o Block in memory.
 * @param lb Lower bound.
 * @param ub Upper bound.
 */
extern void _UF_Constructor(struct UnaryFunction* o, int lb, int ub);

/**
 * @brief Returns the negative unary function value of input value.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target unary function.
 * @param x Input value.
 * @return double Unary function value.
 */
double _UF_Negative_Value_At(struct UnaryFunction* o, double x);

/**
 * @brief Tabulates unary function values for integers within bounds.
 * @param o Target unary function.
 */
extern void UF_Tabulate(struct UnaryFunction* o);

/**
 * @brief Checks whether two unary functions are the same.
 * @param f1 First unary function.
 * @param f2 Second unary function.
 * @param tolerance Value difference maximum tolerance.
 * @return int 1 when equal, 0 otherwise.
 */
extern int UF_STATIC_Same_Functions_For_Ints(struct UnaryFunction* f1, struct UnaryFunction* f2, double tolerance);

/**
 * @brief Initializes a square function instance.
 * @param o Block in memory.
 * @param lb Lower bound.
 * @param ub Upper bound.
 */
extern void S_Constructor(struct Square* o, int lb, int ub);

/**
 * @brief Calculates the square of an input value.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target square function.
 * @param x Input value.
 * @return double Squared value.
 */
extern double _S_Value_At(struct Square* o, double x);

/**
 * @brief Initializes a linear function instance.
 * @param o Block in memory.
 * @param lb Lower bound.
 * @param ub Upper bound.
 * @param a_coef Coefficient a.
 * @param b_coef Coefficient b.
 */
extern void L_Constructor(struct Linear* o, int lb, int ub, double a_coef, double b_coef);

/**
 * @brief Calculates the linear function value.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target linear function.
 * @param x Input value.
 * @return double Linear function value.
 */
extern double _L_Value_At(struct Linear* o, double x);

#endif
