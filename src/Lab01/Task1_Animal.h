#ifndef __TASK1_ANIMAL_H__
#define __TASK1_ANIMAL_H__

#define _ANIMAL_VCALL(o, i, ...) (((struct Animal*)o)->_vtable[i](##__VA_ARGS__))
#define ANIMAL_GREET(o) (_ANIMAL_VCALL(o, 0))
#define ANIMAL_MENU(o) (_ANIMAL_VCALL(o, 1))

/* Virtual functions type. */
typedef const char* (*PTRFUN)(void);

/* Animal structure. */
struct Animal {
    const PTRFUN* const _vtable;
    const char* name;
};

/* Dog structure. */
struct Dog {
    struct Animal _base;
};

/* Cat structure. */
struct Cat {
    struct Animal _base;
};

/**
 * @brief Constructs a dog instance.
 * @param dst Destination in memory.
 * @param name Dog name.
 */
extern void constructDog(struct Dog* dst, const char* name);

/**
 * @brief Constructs a cat instance.
 * @param dst Destination in memory.
 * @param name Cat name.
 */
extern void constructCat(struct Cat* dst, const char* name);

#endif
