#ifndef __TASK_1_ALT_ANIMAL__
#define __TASK_1_ALT_ANIMAL__

#define _A_VCALL(o, i, t, ...) (((t)((struct Animal*)o)->_vtable[i])(o, ##__VA_ARGS__))
#define A_DESTRUCTOR(o) (_A_VCALL(o, 0, VOID_FPTR))
#define A_GREET(o) (_A_VCALL(o, 1, CONST_CHAR_PTR_FPTR))
#define A_MENU(o) (_A_VCALL(o, 2, CONST_CHAR_PTR_FPTR))

/* Virtual function types. */
typedef void* FPTR;
typedef void (*VOID_FPTR)();
typedef const char* (*CONST_CHAR_PTR_FPTR)();

/* Animal "class". */
struct Animal {
    const FPTR* const _vtable;
    const char* const name;
};

/* Dog "class". */
struct Dog {
    struct Animal _base;
};

/* Cat "class". */
struct Cat {
    struct Animal _base;
};

/**
 * @brief Initializes an animal instance.
 * @note Should only be called within constructors of derived classes.
 * @param o Block in memory.
 * @param name Name of the animal.
 */
extern void _A_Constructor(struct Animal* o, const char* name);

/**
 * @brief Destroys an animal instance.
 * @note Should only be called within destructors of derived classes.
 * @param o Target animal.
 */
extern void _A_Destructor(struct Animal* o);

/**
 * @brief Initializes a dog instance.
 * @param o Block in memory.
 * @param name Name of the dog.
 */
extern void D_Constructor(struct Dog* o, const char* name);

/**
 * @brief Destroys a dog instance.
 * @note Should only be called within destructors of derived classes.
 * @param o Target dog.
 */
extern void _D_Destructor(struct Dog* o);

/**
 * @brief Gets dog greeting.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target dog.
 * @return const char* Dog greeting.
 */
extern const char* _D_Greet(struct Dog* o);

/**
 * @brief Gets dog menu.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target dog.
 * @return const char* Dog menu.
 */
extern const char* _D_Menu(struct Dog* o);

/**
 * @brief Initializes a cat instance.
 * @param o Block in memory.
 * @param name Name of the cat.
 */
extern void C_Constructor(struct Cat* o, const char* name);

/**
 * @brief Destroys a cat instance.
 * @note Should only be called within destructors of derived classes.
 * @param o Target cat.
 */
extern void _C_Destructor(struct Cat* o);

/**
 * @brief Gets cat greeting.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target cat.
 * @return const char* Cat greeting.
 */
extern const char* _C_Greet(struct Cat* o);

/**
 * @brief Gets cat menu.
 * @note Should only be used when constructing virtual tables of derived classes that don't override this method.
 * @param o Target cat.
 * @return const char* Cat menu.
 */
extern const char* _C_Menu(struct Cat* o);

#endif
