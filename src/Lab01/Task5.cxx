#include <iostream>

class B {
public:
    virtual int prva() = 0;
    virtual int druga() = 0;
};

class D : public B {
public:
    virtual int prva() { return 0; }
    virtual int druga() { return 42; }
};

typedef int (*FPTR)(void);

void print(B* pb) {
    FPTR* _vtable = *(reinterpret_cast<FPTR**>(pb));
    std::cout << "prva()  : " << (_vtable[0])() << std::endl;
    std::cout << "druga() : " << (_vtable[1])() << std::endl;
}

int main() {
    D d;
    print(&d);
    return 0;
}
