#include <stdlib.h>
#include <stdio.h>
#include "Task2_Functions.h"

static const FPTR _UF_vtable[] = { NULL, _UF_Negative_Value_At };

void _UF_Constructor(struct UnaryFunction* o, int lb, int ub) {

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_vtable)) = _UF_vtable;

    /* Set bounds. */
    o->lower_bound = lb;
    o->upper_bound = ub;

}

double _UF_Negative_Value_At(struct UnaryFunction* o, double x) {

    /* Returns the negative value from a different virtual function. */
    return -UF_VALUE_AT(o, x);

}

void UF_Tabulate(struct UnaryFunction* o) {

    /* Print all unary function values for integers within bounds. */
    for (int x = o->lower_bound; x <= o->upper_bound; x++)
        printf("f(%d)=%lf\n", x, UF_VALUE_AT(o, x));

}

int UF_STATIC_Same_Functions_For_Ints(struct UnaryFunction* f1, struct UnaryFunction* f2, double tolerance) {

    /* Makes sure bounds are equal for both unary functions. */
    if (f1->lower_bound != f2->lower_bound) return 0;
    if (f1->upper_bound != f2->upper_bound) return 0;

    /* Check all unary function values for integers within bounds. */
    for (int x = f1->lower_bound; x <= f1->upper_bound; x++) {
        double delta = UF_VALUE_AT(f1, x) - UF_VALUE_AT(f2, x);
        if (delta < 0) delta = -delta;
        if (delta > tolerance) return 0;
    }

    /* Unary functions are equal. */
    return 1;

}

static const FPTR _S_vtable[] = { _S_Value_At, _UF_Negative_Value_At };

void S_Constructor(struct Square* o, int lb, int ub) {

    /* Call base constructor. */
    _UF_Constructor((struct UnaryFunction*)o, lb, ub);

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_base._vtable)) = _S_vtable;

}

double _S_Value_At(struct Square* o, double x) {

    /* Return squared value. */
    return x * x;

}

static const FPTR _L_vtable[] = { _L_Value_At, _UF_Negative_Value_At };

void L_Constructor(struct Linear* o, int lb, int ub, double a_coef, double b_coef) {

    /* Call base constructor. */
    _UF_Constructor((struct UnaryFunction*)o, lb, ub);

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_base._vtable)) = _L_vtable;

    /* Set coefficients. */
    o->a = a_coef;
    o->b = b_coef;

}

double _L_Value_At(struct Linear* o, double x) {

    /* Return linear function value. */
    return o->a * x + o->b;

}
