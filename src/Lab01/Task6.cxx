#include <stdio.h>

class Base {

public:

    Base() {
        metoda();
    }

    virtual void virtualnaMetoda() {
        printf("ja sam bazna implementacija!\n");
    }

    void metoda() {
        printf("Metoda kaze: ");
        virtualnaMetoda();
    }

};

class Derived : public Base {

public:

    Derived() : Base() {
        metoda();
    }

    virtual void virtualnaMetoda() {
        printf("ja sam izvedena implementacija!\n");
    }
};

int main(){
    Derived* pd = new Derived();
    pd->metoda();
    return 0;
}

/*

Sljed izvodjenja unutar konstruktora:
1. pozovi bazni konstruktor (ako postoji)
2. postavi virtualnu tablicu
3. tijelo konstruktora

Dakle, s obzirom da ce se prvo pozvati Base() konstruktor, on ce postaviti svoju virtualnu tablicu koju ce i koristiti u pozivu metoda() tj. virtualnaMetoda().
Nakon sto on zavrsi, postavlja se virtualna tablica derivirane klase i opet se ona koristi unutar poziva metoda() s pozivom virtualnaMetoda().

*/
