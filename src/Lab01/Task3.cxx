#include <iostream>

class CoolClass {
public:
    virtual void set(int x) { x_ = x; };
    virtual int get() { return x_; };
private:
    int x_;
};

class PlainOldClass {
public:
    void set(int x) { x_ = x; };
    int get() { return x_; };
private:
    int x_;
};

int main() {

    /* Print sizes */
    std::cout << "sizeof(CoolClass) = " << sizeof(CoolClass) << std::endl;
    std::cout << "sizeof(PlainOldClass) = " << sizeof(PlainOldClass) << std::endl;

    return 0;

}

/*

Velicina CoolClass je veca nego velicina PlainOldClass.
Razlog tomu jest sto CoolClass sadrzi virtualne funkcije, dakle mora sadrzavati pokazivac na virtualnu tablicu (te ostale klasne podatke).

Na 64 bitnom kompajleru vraceni rezultati su:
sizeof(CoolClass) = 16
sizeof(PlainOldClass) = 4

Dakle velicina je 4 puta veca.
Razlog toj pojavi jest implicitni padding kojeg prevoditelj ubacuje da poravna razred.

Razred se poravnava na alignment koji je velik koliko i najveci element, a za slucaj CoolClass to je pokazivac od 8 bajtova ili 64 bita.
Dakle, iskoristena velicina bi bila 12 bajta, a padding iznosi 4 bajta.

*/
