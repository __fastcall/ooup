#include "Task1_Animal.h"

/* Dog virtual overrides. */
static const char* dogGreet(void) { return "vau!"; }
static const char* dogMenu(void) { return "kuhanu govedinu"; }

/* Cat virtual overrides. */
static const char* catGreet(void) { return "mijau!"; }
static const char* catMenu(void) { return "konzerviranu tunjevinu"; }

/* Virtual tables. */
static PTRFUN vtable_dog[] = { dogGreet, dogMenu };
static PTRFUN vtable_cat[] = { catGreet, catMenu };

void constructDog(struct Dog* dst, const char* name) {

    /* Initialize virtual table. */
    *((PTRFUN**)&(dst->_base._vtable)) = vtable_dog;

    /* Set animal name. */
    dst->_base.name = name;

}

void constructCat(struct Cat* dst, const char* name) {

    /* Initialize virtual table. */
    *((PTRFUN**)&(dst->_base._vtable)) = vtable_cat;

    /* Set animal name. */
    dst->_base.name = name;

}
