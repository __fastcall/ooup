#include <stdlib.h>
#include <string.h>
#include "Task1alt_Animal.h"

static const FPTR _A_vtable[] = { _A_Destructor, NULL, NULL };

void _A_Constructor(struct Animal* o, const char* name) {

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_vtable)) = _A_vtable;

    /* Copy animal name. */
    size_t len = strlen(name) + 1;
    *((char**)&(o->name)) = (char*)malloc(len * sizeof(char));
    memcpy((char*)o->name, name, len);

}

void _A_Destructor(struct Animal* o) {

    /* Free animal name. */
    free((char*)o->name);
    *((char**)&(o->name)) = NULL;

}

static const FPTR _D_vtable[] = { _D_Destructor, _D_Greet, _D_Menu };

void D_Constructor(struct Dog* o, const char* name) {

    /* Call base constructor. */
    _A_Constructor((struct Animal*)o, name);

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_base._vtable)) = _D_vtable;

}

void _D_Destructor(struct Dog* o) {

    /* Call base destructor. */
    _A_Destructor((struct Animal*)o);

}

const char* _D_Greet(struct Dog* o) {
    
    /* Dog greeting. */
    return "vau!";

}

const char* _D_Menu(struct Dog* o) {

    /* Dog menu. */
    return "kuhanu govedinu";

}

static const FPTR _C_vtable[] = { _C_Destructor, _C_Greet, _C_Menu };

void C_Constructor(struct Cat* o, const char* name) {

    /* Call base constructor. */
    _A_Constructor((struct Animal*)o, name);

    /* Initialize virtual table. */
    *((const FPTR**)&(o->_base._vtable)) = _C_vtable;

}

void _C_Destructor(struct Cat* o) {

    /* Call base destructor. */
    _A_Destructor((struct Animal*)o);

}

const char* _C_Greet(struct Cat* o) {
    
    /* Dog greeting. */
    return "mijau!";

}

const char* _C_Menu(struct Cat* o) {

    /* Dog menu. */
    return "konzerviranu tunjevinu";

}
