#include <stdlib.h>
#include <stdio.h>
#include "Task2_Functions.h"

int main() {

    /* UnaryFunction* f1 = new Square(-2, 2); */
    struct UnaryFunction* f1 = (struct UnaryFunction*)malloc(sizeof(struct Square));
    S_Constructor((struct Square*)f1, -2, 2);

    /* f1->Tabulate(); */
    UF_Tabulate(f1);

    /* UnaryFunction* f1 = new Linear(-2, 2, 5, -2); */
    struct UnaryFunction* f2 = (struct UnaryFunction*)malloc(sizeof(struct Linear));
    L_Constructor((struct Linear*)f2, -2, 2, 5, -2);

    /* f2->Tabulate(); */
    UF_Tabulate(f2);

    /* printf("f1==f2: %s\n", UnaryFunction::Same_Functions_For_Ints(f1, f2, 1e-6) ? "DA" : "NE"); */
    printf("f1==f2: %s\n", UF_STATIC_Same_Functions_For_Ints(f1, f2, 1e-6) ? "DA" : "NE");

    /* printf("neg_val f2(1) = %lf\n", f2->Negative_Value_At(1.0)); */
    printf("neg_val f2(1) = %lf\n", UF_NEGATIVE_VALUE_AT(f2, 1.0));

    /* delete f1; delete f2; */
    free(f1); free(f2);

    return 0;

}
