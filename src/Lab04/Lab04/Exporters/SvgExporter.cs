﻿using System;
using System.Drawing;
using System.Xml;
using Lab04.Controls;

namespace Lab04.Exporters
{
    public class SvgExporter : ICanvasRenderer, IDisposable
    {
        public Color LineColor { get; set; } = Color.Black;
        public Color FillColor { get; set; } = Color.White;

        private XmlWriter _xmlWriter;

        public SvgExporter(string filename)
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;
            xmlWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;
            _xmlWriter = XmlWriter.Create(filename, xmlWriterSettings);
            _xmlWriter.WriteStartElement("svg", @"http://www.w3.org/2000/svg");
            _xmlWriter.WriteAttributeString("xmlns", "xlink", null, @"http://www.w3.org/1999/xlink");
        }

        public void DrawLine(Point start, Point end)
        {
            _xmlWriter.WriteStartElement("line");
            _xmlWriter.WriteAttributeString("x1", start.X.ToString());
            _xmlWriter.WriteAttributeString("y1", start.Y.ToString());
            _xmlWriter.WriteAttributeString("x2", end.X.ToString());
            _xmlWriter.WriteAttributeString("y2", end.Y.ToString());
            _xmlWriter.WriteAttributeString("style", "stroke:" + ColorToHexString(LineColor) + ";");
            _xmlWriter.WriteEndElement();
        }

        public void DrawOval(Point center, int horizontalRadius, int verticalRadius)
        {
            _xmlWriter.WriteStartElement("ellipse");
            _xmlWriter.WriteAttributeString("cx", center.X.ToString());
            _xmlWriter.WriteAttributeString("cy", center.Y.ToString());
            _xmlWriter.WriteAttributeString("rx", horizontalRadius.ToString());
            _xmlWriter.WriteAttributeString("ry", verticalRadius.ToString());
            _xmlWriter.WriteAttributeString("style", "stroke:" + ColorToHexString(LineColor) + ";fill-opacity:0;");
            _xmlWriter.WriteEndElement();
        }

        public void DrawOval(Rectangle boundingBox)
        {
            Point center = new Point((boundingBox.Left + boundingBox.Right) / 2, (boundingBox.Top + boundingBox.Bottom) / 2);
            int horizontalRaidus = Math.Abs(center.X - boundingBox.Left);
            int verticalRadius = Math.Abs(center.Y - boundingBox.Top);
            DrawOval(center, horizontalRaidus, verticalRadius);
        }

        public void DrawRectangle(Rectangle boundingBox)
        {
            _xmlWriter.WriteStartElement("rect");
            _xmlWriter.WriteAttributeString("x", boundingBox.X.ToString());
            _xmlWriter.WriteAttributeString("y", boundingBox.Y.ToString());
            _xmlWriter.WriteAttributeString("width", boundingBox.Width.ToString());
            _xmlWriter.WriteAttributeString("height", boundingBox.Height.ToString());
            _xmlWriter.WriteAttributeString("style", "stroke:" + ColorToHexString(LineColor) + ";fill-opacity:0;");
            _xmlWriter.WriteEndElement();
        }

        public void FillOval(Point center, int horizontalRadius, int verticalRadius)
        {
            _xmlWriter.WriteStartElement("ellipse");
            _xmlWriter.WriteAttributeString("cx", center.X.ToString());
            _xmlWriter.WriteAttributeString("cy", center.Y.ToString());
            _xmlWriter.WriteAttributeString("rx", horizontalRadius.ToString());
            _xmlWriter.WriteAttributeString("ry", verticalRadius.ToString());
            _xmlWriter.WriteAttributeString("style", "fill:" + ColorToHexString(FillColor) + ";stroke-opacity:0;");
            _xmlWriter.WriteEndElement();
        }

        public void FillOval(Rectangle boundingBox)
        {
            Point center = new Point((boundingBox.Left + boundingBox.Right) / 2, (boundingBox.Top + boundingBox.Bottom) / 2);
            int horizontalRaidus = Math.Abs(center.X - boundingBox.Left);
            int verticalRadius = Math.Abs(center.Y - boundingBox.Top);
            FillOval(center, horizontalRaidus, verticalRadius);
        }

        public void FillRectangle(Rectangle boundingBox)
        {
            _xmlWriter.WriteStartElement("rect");
            _xmlWriter.WriteAttributeString("x", boundingBox.X.ToString());
            _xmlWriter.WriteAttributeString("y", boundingBox.Y.ToString());
            _xmlWriter.WriteAttributeString("width", boundingBox.Width.ToString());
            _xmlWriter.WriteAttributeString("height", boundingBox.Height.ToString());
            _xmlWriter.WriteAttributeString("style", "fill:" + ColorToHexString(FillColor) + ";stroke-opacity:0;");
            _xmlWriter.WriteEndElement();
        }

        public void FillPolygon(params Point[] points)
        {
            _xmlWriter.WriteStartElement("polygon");
            string str = string.Empty;
            foreach (Point point in points)
            {
                str += point.X.ToString() + "," + point.Y.ToString() + " ";
            }
            _xmlWriter.WriteAttributeString("points", str);
            _xmlWriter.WriteAttributeString("style", "fill:" + ColorToHexString(FillColor) + ";stroke-opacity:0;");
            _xmlWriter.WriteEndElement();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _xmlWriter.WriteEndElement();
            _xmlWriter.Close();
        }

        private string ColorToHexString(Color color)
        {
            return "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }

        ~SvgExporter()
        {
            Dispose(false);
        }
    }
}
