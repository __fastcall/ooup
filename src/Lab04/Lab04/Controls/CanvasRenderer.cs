﻿using System.Drawing;

namespace Lab04.Controls
{
    public class CanvasRenderer : ICanvasRenderer
    {
        public Pen LinePen { get; set; } = Pens.Black;
        public Brush FillBrush { get; set; } = Brushes.White;

        private Graphics _graphics;

        public CanvasRenderer(Graphics graphics)
        {
            _graphics = graphics;
        }

        public void DrawLine(Point start, Point end)
        {
            _graphics.DrawLine(LinePen, start, end);
        }

        public void DrawOval(Point center, int horizontalRadius, int verticalRadius)
        {
            _graphics.DrawEllipse(LinePen, new Rectangle(new Point(center.X - horizontalRadius, center.Y - verticalRadius), new Size(2 * horizontalRadius, 2 * verticalRadius)));
        }

        public void DrawOval(Rectangle boundingBox)
        {
            _graphics.DrawEllipse(LinePen, boundingBox);
        }

        public void DrawRectangle(Rectangle boundingBox)
        {
            _graphics.DrawRectangle(LinePen, boundingBox);
        }

        public void FillOval(Point center, int horizontalRadius, int verticalRadius)
        {
            _graphics.FillEllipse(FillBrush, new Rectangle(new Point(center.X - horizontalRadius, center.Y - verticalRadius), new Size(2 * horizontalRadius, 2 * verticalRadius)));
        }

        public void FillOval(Rectangle boundingBox)
        {
            _graphics.FillEllipse(FillBrush, boundingBox);
        }

        public void FillRectangle(Rectangle boundingBox)
        {
            _graphics.FillRectangle(FillBrush, boundingBox);
        }

        public void FillPolygon(params Point[] points)
        {
            _graphics.FillPolygon(FillBrush, points);
        }
    }
}
