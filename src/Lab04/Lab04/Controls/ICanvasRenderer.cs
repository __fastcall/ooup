﻿using System.Drawing;

namespace Lab04.Controls
{
    public interface ICanvasRenderer
    {
        void DrawLine(Point start, Point end);

        void DrawOval(Point center, int horizontalRadius, int verticalRadius);

        void DrawOval(Rectangle boundingBox);

        void DrawRectangle(Rectangle boundingBox);

        void FillOval(Point center, int horizontalRadius, int verticalRadius);

        void FillOval(Rectangle boundingBox);

        void FillRectangle(Rectangle boundingBox);

        void FillPolygon(params Point[] points);
    }
}
