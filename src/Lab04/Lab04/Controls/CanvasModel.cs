﻿using Lab04.Drawing;
using Lab04.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;

namespace Lab04.Controls
{
    public class CanvasModel : IDisposable
    {
        public double SelectionProximity
        {
            get { return _selectionProximity; }
            set { if (value <= 0.0) throw new ArgumentException(); _selectionProximity = value; }
        }
        public ICollection<GraphicalObject> Objects { get => _objects; }
        public IEnumerable<GraphicalObject> SelectedObjects { get => Objects.Where(o => o.Selected); }
        public event EventHandler Changed;

        private double _selectionProximity = 10.0;
        private ObservableCollection<GraphicalObject> _objects = new ObservableCollection<GraphicalObject>();

        public CanvasModel()
        {
            _objects.CollectionChanged += Objects_CollectionChanged;
        }

        public GraphicalObject FindSelectedObject(Point point)
        {
            GraphicalObject nearestObject = null;
            double distance = SelectionProximity;
            foreach (GraphicalObject o in Objects)
            {
                double d = o.GetDistanceFrom(point);
                if (d < distance)
                {
                    nearestObject = o;
                    distance = d;
                }
            }
            return nearestObject;
        }

        public SelectablePoint FindSelectedHotPoint(GraphicalObject o, Point point)
        {
            SelectablePoint nearestHotPoint = null;
            double distance = SelectionProximity;
            foreach (SelectablePoint hotPoint in o.HotPoints)
            {
                double d = GeometryUtil.CalculatePointDistance(hotPoint.Get(), point);
                if (d < distance)
                {
                    nearestHotPoint = hotPoint;
                    distance = d;
                }
            }
            return nearestHotPoint;
        }

        public void MakeCloser(GraphicalObject o)
        {
            int index = _objects.IndexOf(o);
            _objects.Move(index, Math.Min(_objects.Count - 1, index + 1));
        }

        public void MakeClosest(GraphicalObject o)
        {
            _objects.Move(_objects.IndexOf(o), _objects.Count - 1);
        }

        public void MakeFurther(GraphicalObject o)
        {
            int index = _objects.IndexOf(o);
            _objects.Move(index, Math.Max(0, index - 1));
        }

        public void MakeFurthest(GraphicalObject o)
        {
            _objects.Move(_objects.IndexOf(o), 0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Objects.Clear();
        }

        private void Objects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (GraphicalObject o in e.OldItems)
                {
                    o.Changed -= GraphicalObject_Changed;
                }
            }
            if (e.NewItems != null)
            {
                foreach (GraphicalObject o in e.NewItems)
                {
                    o.Changed += GraphicalObject_Changed;
                }
            }
            Changed?.Invoke(this, EventArgs.Empty);
        }

        private void GraphicalObject_Changed(object sender, EventArgs e)
        {
            Changed?.Invoke(sender, e);
        }

        ~CanvasModel()
        {
            Dispose(false);
        }
    }
}
