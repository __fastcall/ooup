﻿using Lab04.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;

namespace Lab04.Controls
{
    public class CompositeShape : GraphicalObject, IDisposable
    {
        public const string ShapeID = "@COMP";
        public ICollection<GraphicalObject> Objects { get => _objects; }

        private ObservableCollection<GraphicalObject> _objects = new ObservableCollection<GraphicalObject>();

        public CompositeShape() : base(0)
        {
            _objects.CollectionChanged += Objects_CollectionChanged;
        }

        public CompositeShape(params GraphicalObject[] objects) : this()
        {
            foreach (GraphicalObject o in objects)
            {
                Objects.Add(o);
            }
        }

        public CompositeShape(CompositeShape compositeShape) : this()
        {
            foreach (GraphicalObject o in compositeShape.Objects)
            {
                _objects.Add(o.Clone());
            }
        }

        public override void Translate(Point point)
        {
            foreach (GraphicalObject o in Objects)
            {
                o.Translate(point);
            }
        }

        public override Rectangle GetBoundingBox()
        {
            int left = int.MaxValue, right = int.MinValue, top = int.MaxValue, bottom = int.MinValue;
            foreach (GraphicalObject o in Objects)
            {
                Rectangle boundingBox = o.GetBoundingBox();
                left = Math.Min(left, boundingBox.Left);
                right = Math.Max(right, boundingBox.Right);
                top = Math.Min(top, boundingBox.Top);
                bottom = Math.Max(bottom, boundingBox.Bottom);
            }
            return new Rectangle(new Point(left, top), new Size(right - left, bottom - top));
        }

        public override double GetDistanceFrom(Point point)
        {
            Rectangle boundingBox = GetBoundingBox();
            return GeometryUtil.CalculatePointDistance(new Point((boundingBox.Left + boundingBox.Right) / 2, (boundingBox.Top + boundingBox.Bottom) / 2), point);
        }

        public override void Render(ICanvasRenderer canvasRenderer)
        {
            foreach (GraphicalObject o in Objects)
            {
                o.Render(canvasRenderer);
            }
        }

        public override string GetShapeName()
        {
            return "KompozitniOblik";
        }

        public override GraphicalObject Clone()
        {
            return new CompositeShape(this);
        }

        public override string GetShapeID()
        {
            return ShapeID;
        }

        public override void Save(ICollection<string> lines)
        {
            foreach (GraphicalObject o in Objects)
            {
                o.Save(lines);
            }
            lines.Add(GetShapeID() + " " + Objects.Count.ToString());
        }

        public override void Load(Stack<string> stack, string data)
        {
            int count = int.Parse(data);
            List<GraphicalObject> objects = new List<GraphicalObject>();
            for (int i = 0; i < count; ++i)
            {
                objects.Add(GraphicalObjectFactory.Load(stack));
            }
            objects.Reverse();
            foreach (GraphicalObject o in objects)
            {
                Objects.Add(o);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Objects.Clear();
        }

        private void Objects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (GraphicalObject o in e.OldItems)
                {
                    o.Changed -= GraphicalObject_Changed;
                }
            }
            if (e.NewItems != null)
            {
                foreach (GraphicalObject o in e.NewItems)
                {
                    o.Changed += GraphicalObject_Changed;
                }
            }
            InvokeChanged(this, EventArgs.Empty);
        }

        private void GraphicalObject_Changed(object sender, EventArgs e)
        {
            InvokeChanged(sender, e);
        }

        ~CompositeShape()
        {
            Dispose(false);
        }
    }
}
