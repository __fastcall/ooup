﻿using Lab04.Drawing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lab04.Controls
{
    public class Canvas : Control
    {
        public CanvasModel Model
        {
            get { if (_model == null) Model = new CanvasModel(); return _model; }
            set { if (_model != null) _model.Changed -= Model_Changed; ; _model = value; if (_model != null) _model.Changed += Model_Changed; }
        }
        public ICollection<GraphicalObject> Objects
        {
            get { return Model.Objects; }
        }
        public IEnumerable<GraphicalObject> SelectedObjects
        {
            get { return Model.SelectedObjects; }
        }
        public event EventHandler<GraphicalObjectDrawnEventArgs> GraphicalObjectDrawn;
        public event EventHandler CanvasComplete;

        private CanvasModel _model;

        public Canvas() { }

        public Canvas(CanvasModel model)
        {
            Model = model;
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected override void Dispose(bool disposing)
        {
            Model = null;
            base.Dispose(disposing);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            CanvasRenderer renderer = new CanvasRenderer(e.Graphics);
            foreach (GraphicalObject o in Objects)
            {
                renderer.LinePen = Pens.Red;
                renderer.FillBrush = Brushes.Blue;
                o.Render(renderer);
                renderer.LinePen = Pens.DarkCyan;
                renderer.FillBrush = Brushes.White;
                GraphicalObjectDrawn?.Invoke(renderer, new GraphicalObjectDrawnEventArgs(o));
            }
            renderer.LinePen = Pens.DarkCyan;
            renderer.FillBrush = Brushes.White;
            CanvasComplete?.Invoke(renderer, EventArgs.Empty);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                case Keys.Up | Keys.Shift:
                case Keys.Down | Keys.Shift:
                case Keys.Left | Keys.Shift:
                case Keys.Right | Keys.Shift:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void Model_Changed(object sender, EventArgs e)
        {
            Invalidate();
        }

        ~Canvas()
        {
            Dispose(false);
        }
    }
}
