﻿using Lab04.Collections.Generic;
using Lab04.Drawing;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Lab04.Controls
{
    public abstract class GraphicalObject
    {
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; SelectedChanged?.Invoke(this, EventArgs.Empty); }
        }
        public event EventHandler SelectedChanged;
        public IndexableArray<SelectablePoint> HotPoints { get; }
        public event EventHandler HotPointsChanged;
        public event EventHandler Changed;

        private bool _selected = false;
        private SelectablePoint[] _hotPoints;

        public GraphicalObject(int hotPointCount)
        {
            if (hotPointCount < 0) throw new ArgumentException();
            SelectedChanged += GraphicalObject_AnyChanged;
            HotPointsChanged += GraphicalObject_AnyChanged;
            _hotPoints = new SelectablePoint[hotPointCount];
            for (int i = 0; i < _hotPoints.Length; ++i)
            {
                _hotPoints[i] = new SelectablePoint();
                _hotPoints[i].Changed += HotPoints_Changed;
            }
            HotPoints = new IndexableArray<SelectablePoint>(_hotPoints);
        }

        public virtual void Translate(Point point)
        {
            foreach (SelectablePoint hotPoint in HotPoints)
            {
                hotPoint.Translate(point);
            }
        }

        public abstract Rectangle GetBoundingBox();

        public abstract double GetDistanceFrom(Point point);

        public abstract void Render(ICanvasRenderer canvasRenderer);

        public abstract string GetShapeName();

        public abstract GraphicalObject Clone();

        public abstract string GetShapeID();

        public abstract void Save(ICollection<string> lines);

        public abstract void Load(Stack<string> stack, string data);

        protected void InvokeChanged(object sender, EventArgs e)
        {
            Changed?.Invoke(sender, e);
        }

        private void GraphicalObject_AnyChanged(object sender, EventArgs e)
        {
            Changed?.Invoke(sender, e);
        }

        private void HotPoints_Changed(object sender, EventArgs e)
        {
            HotPointsChanged?.Invoke(sender, e);
        }
    }
}
