﻿using Lab04.Drawing;
using Lab04.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Lab04.Controls
{
    public class Oval : GraphicalObject
    {
        public const string ShapeID = "@OVAL";
        public SelectablePoint HorizontalHotPoint { get; }
        public SelectablePoint VerticalHotPoint { get; }

        public Oval() : base(2)
        {
            HorizontalHotPoint = HotPoints[0];
            VerticalHotPoint = HotPoints[1];
        }

        public Oval(Point horizontalHotPoint, Point verticalHotPoint) : this()
        {
            HorizontalHotPoint.Set(horizontalHotPoint);
            VerticalHotPoint.Set(verticalHotPoint);
        }

        public Oval(Oval oval) : this(oval.HorizontalHotPoint.Get(), oval.VerticalHotPoint.Get()) { }

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle(
                new Point((HorizontalHotPoint.X > VerticalHotPoint.X) ? (2 * VerticalHotPoint.X - HorizontalHotPoint.X) : HorizontalHotPoint.X, (VerticalHotPoint.Y > HorizontalHotPoint.Y) ? (2 * HorizontalHotPoint.Y - VerticalHotPoint.Y) : VerticalHotPoint.Y),
                new Size(2 * Math.Abs(HorizontalHotPoint.X - VerticalHotPoint.X), 2 * Math.Abs(HorizontalHotPoint.Y - VerticalHotPoint.Y))
            );
        }

        public override double GetDistanceFrom(Point point)
        {
            return GeometryUtil.CalculatePointDistance(new Point(VerticalHotPoint.X, HorizontalHotPoint.Y), point);
        }

        public override void Render(ICanvasRenderer canvasRenderer)
        {
            Rectangle boundingBox = GetBoundingBox();
            canvasRenderer.FillOval(boundingBox);
            canvasRenderer.DrawOval(boundingBox);
        }

        public override string GetShapeName()
        {
            return "Oval";
        }

        public override GraphicalObject Clone()
        {
            return new Oval(this);
        }

        public override string GetShapeID()
        {
            return ShapeID;
        }

        public override void Save(ICollection<string> lines)
        {
            lines.Add(GetShapeID() + " " + HorizontalHotPoint.X.ToString() + " " + HorizontalHotPoint.Y.ToString() + " " + VerticalHotPoint.X.ToString() + " " + VerticalHotPoint.Y.ToString());
        }

        public override void Load(Stack<string> stack, string data)
        {
            string[] par = data.Split(' ');
            HorizontalHotPoint.X = int.Parse(par[0]);
            HorizontalHotPoint.Y = int.Parse(par[1]);
            VerticalHotPoint.X = int.Parse(par[2]);
            VerticalHotPoint.Y = int.Parse(par[3]);
        }
    }
}
