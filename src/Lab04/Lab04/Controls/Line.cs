﻿using Lab04.Drawing;
using Lab04.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Lab04.Controls
{
    public class Line : GraphicalObject
    {
        public const string ShapeID = "@LINE";
        public SelectablePoint StartHotPoint { get; }
        public SelectablePoint EndHotPoint { get; }

        public Line() : base(2)
        {
            StartHotPoint = HotPoints[0];
            EndHotPoint = HotPoints[1];
        }

        public Line(Point start, Point end) : this()
        {
            StartHotPoint.Set(start);
            EndHotPoint.Set(end);
        }

        public Line(Line line) : this(line.StartHotPoint.Get(), line.EndHotPoint.Get()) { }

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle(
                new Point((StartHotPoint.X < EndHotPoint.X) ? StartHotPoint.X : EndHotPoint.X, (StartHotPoint.Y < EndHotPoint.Y) ? StartHotPoint.Y : EndHotPoint.Y),
                new Size(Math.Abs(StartHotPoint.X - EndHotPoint.X), Math.Abs(StartHotPoint.Y - EndHotPoint.Y))
            );
        }

        public override double GetDistanceFrom(Point point)
        {
            return GeometryUtil.CalculateLineDistance(StartHotPoint.Get(), EndHotPoint.Get(), point);
        }

        public override void Render(ICanvasRenderer canvasRenderer)
        {
            canvasRenderer.DrawLine(StartHotPoint.Get(), EndHotPoint.Get());
        }

        public override string GetShapeName()
        {
            return "Linija";
        }

        public override GraphicalObject Clone()
        {
            return new Line(this);
        }

        public override string GetShapeID()
        {
            return ShapeID;
        }

        public override void Save(ICollection<string> lines)
        {
            lines.Add(GetShapeID() + " " + StartHotPoint.X.ToString() + " " + StartHotPoint.Y.ToString() + " " + EndHotPoint.X.ToString() + " " + EndHotPoint.Y.ToString());
        }

        public override void Load(Stack<string> stack, string data)
        {
            string[] par = data.Split(' ');
            StartHotPoint.X = int.Parse(par[0]);
            StartHotPoint.Y = int.Parse(par[1]);
            EndHotPoint.X = int.Parse(par[2]);
            EndHotPoint.Y = int.Parse(par[3]);
        }
    }
}
