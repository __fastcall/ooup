﻿using System;

namespace Lab04.Controls
{
    public class GraphicalObjectDrawnEventArgs : EventArgs
    {
        public GraphicalObject Target { get; set; }

        public GraphicalObjectDrawnEventArgs(GraphicalObject target)
        {
            Target = target;
        }
    }
}
