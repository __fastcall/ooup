﻿using System;

namespace Lab04.Drawing
{
    public class SelectablePoint : ObservablePoint
    {
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; SelectedChanged?.Invoke(this, EventArgs.Empty); }
        }
        public event EventHandler SelectedChanged;
        public event EventHandler Changed;

        private bool _selected = false;

        public SelectablePoint()
        {
            ValueChanged += SelectablePoint_AnyChanged;
            SelectedChanged += SelectablePoint_AnyChanged;
        }

        private void SelectablePoint_AnyChanged(object sender, EventArgs e)
        {
            Changed?.Invoke(sender, e);
        }
    }
}
