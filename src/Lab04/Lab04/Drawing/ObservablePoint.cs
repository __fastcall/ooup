﻿using System;
using System.Drawing;

namespace Lab04.Drawing
{
    public class ObservablePoint
    {
        public int X
        {
            get { return _x; }
            set { _x = value; ValueChanged?.Invoke(this, EventArgs.Empty); }
        }
        public int Y
        {
            get { return _y; }
            set { _y = value; ValueChanged?.Invoke(this, EventArgs.Empty); }
        }
        public event EventHandler ValueChanged;

        private int _x;
        private int _y;

        public ObservablePoint() { }

        public ObservablePoint(Point point)
        {
            _x = point.X;
            _y = point.Y;
        }

        public Point Get()
        {
            return new Point(X, Y);
        }

        public void Set(Point point)
        {
            _x = point.X;
            Y = point.Y;
        }

        public void Translate(Point point)
        {
            _x += point.X;
            Y += point.Y;
        }
    }
}
