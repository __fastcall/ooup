﻿using Lab04.Controls;
using Lab04.Exporters;
using Lab04.States;
using Lab04.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Lab04
{
    public class FormMain : Form, IStateMachine
    {
        private IState _currentState;
        private Canvas _canvasMain;
        private ImageList _imageListMain;
        private ToolBar _toolBarMain;
        private ToolBarButton _toolBarButtonLoad;
        private ToolBarButton _toolBarButtonSave;
        private ToolBarButton _toolBarButtonSvgExport;
        private ToolBarButton _toolBarButtonLine;
        private ToolBarButton _toolBarButtonOval;
        private ToolBarButton _toolBarButtonSelect;
        private ToolBarButton _toolBarButtonEraser;
        private bool _isMouseDown = false;

        public FormMain()
        {
            // Initialize current state
            _currentState = new IdleState(this);
            _currentState.OnEnter();

            // Create main canvas
            _canvasMain = new Canvas();
            _canvasMain.Dock = DockStyle.Fill;
            _canvasMain.MouseDown += CanvasMain_MouseDown;
            _canvasMain.MouseUp += CanvasMain_MouseUp;
            _canvasMain.MouseMove += CanvasMain_MouseMove;
            _canvasMain.KeyDown += CanvasMain_KeyDown;
            _canvasMain.GraphicalObjectDrawn += CanvasMain_GraphicalObjectDrawn;
            _canvasMain.CanvasComplete += CanvasMain_CanvasComplete;
            Controls.Add(_canvasMain);

            // Create main image list
            _imageListMain = new ImageList();
            _imageListMain.ImageSize = new Size(32, 32);
            _imageListMain.ColorDepth = ColorDepth.Depth24Bit;
            _imageListMain.Images.Add(Properties.Resources.IconLoad);
            _imageListMain.Images.Add(Properties.Resources.IconSave);
            _imageListMain.Images.Add(Properties.Resources.IconSvgExport);
            _imageListMain.Images.Add(Properties.Resources.IconLine);
            _imageListMain.Images.Add(Properties.Resources.IconOval);
            _imageListMain.Images.Add(Properties.Resources.IconSelect);
            _imageListMain.Images.Add(Properties.Resources.IconEraser);

            // Create main tool bar
            _toolBarMain = new ToolBar();
            _toolBarMain.Dock = DockStyle.Top;
            _toolBarMain.ImageList = _imageListMain;
            _toolBarMain.ButtonClick += ToolBarMain_ButtonClick;
            Controls.Add(_toolBarMain);

            // Create load button
            _toolBarButtonLoad = new ToolBarButton();
            _toolBarButtonLoad.Text = "Učitaj";
            _toolBarButtonLoad.ImageIndex = 0;
            _toolBarMain.Buttons.Add(_toolBarButtonLoad);

            // Create save button
            _toolBarButtonSave = new ToolBarButton();
            _toolBarButtonSave.Text = "Pohrani";
            _toolBarButtonSave.ImageIndex = 1;
            _toolBarMain.Buttons.Add(_toolBarButtonSave);

            // Create SVG export button
            _toolBarButtonSvgExport = new ToolBarButton();
            _toolBarButtonSvgExport.Text = "SVG export";
            _toolBarButtonSvgExport.ImageIndex = 2;
            _toolBarMain.Buttons.Add(_toolBarButtonSvgExport);

            // Create line button
            _toolBarButtonLine = new ToolBarButton();
            _toolBarButtonLine.Text = "Linija";
            _toolBarButtonLine.ImageIndex = 3;
            _toolBarButtonLine.Style = ToolBarButtonStyle.ToggleButton;
            _toolBarMain.Buttons.Add(_toolBarButtonLine);

            // Create oval button
            _toolBarButtonOval = new ToolBarButton();
            _toolBarButtonOval.Text = "Oval";
            _toolBarButtonOval.ImageIndex = 4;
            _toolBarButtonOval.Style = ToolBarButtonStyle.ToggleButton;
            _toolBarMain.Buttons.Add(_toolBarButtonOval);

            // Create select button
            _toolBarButtonSelect = new ToolBarButton();
            _toolBarButtonSelect.Text = "Selektiraj";
            _toolBarButtonSelect.ImageIndex = 5;
            _toolBarButtonSelect.Style = ToolBarButtonStyle.ToggleButton;
            _toolBarMain.Buttons.Add(_toolBarButtonSelect);

            // Create eraser button
            _toolBarButtonEraser = new ToolBarButton();
            _toolBarButtonEraser.Text = "Brisalo";
            _toolBarButtonEraser.ImageIndex = 6;
            _toolBarButtonEraser.Style = ToolBarButtonStyle.ToggleButton;
            _toolBarMain.Buttons.Add(_toolBarButtonEraser);

            // Form settings
            Text = "Vector Editor";
            Icon = Properties.Resources.IconShapes;
            StartPosition = FormStartPosition.CenterScreen;
            MinimumSize = new Size(640, 480);
            Size = new Size(800, 600);
        }

        public void SwitchState(IState newState)
        {
            _toolBarButtonLine.Pushed = false;
            _toolBarButtonOval.Pushed = false;
            _toolBarButtonSelect.Pushed = false;
            _toolBarButtonEraser.Pushed = false;
            _currentState.OnLeave();
            _currentState = newState;
            _currentState.OnEnter();
        }

        private void CanvasMain_MouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
            _currentState.OnMouseDown(e.Location, ModifierKeys.HasFlag(Keys.Shift), ModifierKeys.HasFlag(Keys.Control));
        }

        private void CanvasMain_MouseUp(object sender, MouseEventArgs e)
        {
            _isMouseDown = false;
            _currentState.OnMouseUp(e.Location, ModifierKeys.HasFlag(Keys.Shift), ModifierKeys.HasFlag(Keys.Control));
        }

        private void CanvasMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                _currentState.OnMouseDrag(e.Location);
            }
        }

        private void CanvasMain_KeyDown(object sender, KeyEventArgs e)
        {
            _currentState.OnKeyPressed(e.KeyCode, e.Modifiers);
        }

        private void CanvasMain_GraphicalObjectDrawn(object sender, GraphicalObjectDrawnEventArgs e)
        {
            _currentState.OnDrawn((ICanvasRenderer)sender, e.Target);
        }

        private void CanvasMain_CanvasComplete(object sender, EventArgs e)
        {
            _currentState.OnDrawn((ICanvasRenderer)sender);
        }

        private void ToolBarMain_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if ((e.Button.Style == ToolBarButtonStyle.ToggleButton) && (e.Button.Pushed))
            {
                if (e.Button == _toolBarButtonLine)
                {
                    SwitchState(new AddShapeState(this, _canvasMain, new Line(new Point(0, 0), new Point(32, 32))));
                }
                else if (e.Button == _toolBarButtonOval)
                {
                    SwitchState(new AddShapeState(this, _canvasMain, new Oval(new Point(32, 16), new Point(16, 32))));
                }
                else if (e.Button == _toolBarButtonSelect)
                {
                    SwitchState(new SelectShapeState(this, _canvasMain));
                }
                else if (e.Button == _toolBarButtonEraser)
                {
                    SwitchState(new EraserState(this, _canvasMain));
                }
                _toolBarButtonLine.Pushed = (_toolBarButtonLine == e.Button);
                _toolBarButtonOval.Pushed = (_toolBarButtonOval == e.Button);
                _toolBarButtonSelect.Pushed = (_toolBarButtonSelect == e.Button);
                _toolBarButtonEraser.Pushed = (_toolBarButtonEraser == e.Button);
            }
            else if (e.Button == _toolBarButtonLoad)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Title = "Open native file";
                ofd.Filter = "Native text file (*.txt)|*.txt";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Stack<string> stack = new Stack<string>(File.ReadAllLines(ofd.FileName).Where(l => l.Length > 0));
                        List<GraphicalObject> objects = new List<GraphicalObject>();
                        while (stack.Count > 0)
                        {
                            GraphicalObject o = GraphicalObjectFactory.Load(stack);
                            objects.Add(o);
                        }
                        objects.Reverse();
                        _canvasMain.Objects.Clear();
                        foreach (GraphicalObject o in objects)
                        {
                            _canvasMain.Objects.Add(o);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("File parsing failed:" + Environment.NewLine + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (e.Button == _toolBarButtonSave)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Save native file";
                sfd.Filter = "Native text file (*.txt)|*.txt";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    List<string> lines = new List<string>();
                    foreach (GraphicalObject o in _canvasMain.Objects)
                    {
                        o.Save(lines);
                    }
                    File.WriteAllLines(sfd.FileName, lines);
                }
            }
            else if (e.Button == _toolBarButtonSvgExport)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Export SVG File";
                sfd.Filter = "Scalable Vector Graphics (*.svg)|*.svg";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    using (SvgExporter svgExporter = new SvgExporter(sfd.FileName))
                    {
                        svgExporter.LineColor = Color.Red;
                        svgExporter.FillColor = Color.Blue;
                        foreach (GraphicalObject o in _canvasMain.Objects)
                        {
                            o.Render(svgExporter);
                        }
                    }
                }
            }
        }
    }
}
