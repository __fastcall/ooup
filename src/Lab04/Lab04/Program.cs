﻿using System;
using System.Windows.Forms;

namespace Lab04
{
    internal static class Program
    {
        [STAThread]
        private static void Main(string[] arguments)
        {
            Application.EnableVisualStyles();
            Application.Run(new FormMain());
        }
    }
}
