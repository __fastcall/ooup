﻿using Lab04.Controls;
using System.Collections.Generic;

namespace Lab04.Utilities
{
    public static class GraphicalObjectFactory
    {
        public static GraphicalObject Load(Stack<string> stack)
        {
            string line = stack.Pop();
            string opcode = line.Split(' ')[0];
            string data = line.Remove(0, opcode.Length + 1);
            GraphicalObject result = null;
            switch (opcode)
            {
                case Line.ShapeID:
                    result = new Line();
                    break;
                case Oval.ShapeID:
                    result = new Oval();
                    break;
                case CompositeShape.ShapeID:
                    result = new CompositeShape();
                    break;
            }
            result?.Load(stack, data);
            return result;
        }
    }
}
