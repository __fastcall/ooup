﻿using System;
using System.Drawing;

namespace Lab04.Utilities
{
    public static class GeometryUtil
    {
        public static double CalculatePointDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2.0) + Math.Pow(p1.Y - p2.Y, 2.0));
        }

        public static double CalculateLineDistance(Point start, Point end, Point point)
        {
            int dx = end.X - start.X;
            int dy = end.Y - start.Y;
            if ((dx == 0) && (dy == 0))
            {
                return CalculatePointDistance(start, point);
            }
            double t = (double)((point.X - start.X) * dx + (point.Y - start.Y) * dy) / (dx * dx + dy * dy);
            if (t < 0.0)
            {
                return CalculatePointDistance(start, point);
            }
            else if (t > 1.0)
            {
                return CalculatePointDistance(end, point);
            }
            return CalculatePointDistance(new Point(start.X + (int)(t * dx), start.Y + (int)(t * dy)), point);
        }
    }
}
