﻿using System.Collections;
using System.Collections.Generic;

namespace Lab04.Collections.Generic
{
    public class IndexableArray<T> : IEnumerable<T>
    {
        public T this[int index]
        {
            get { return _array[index]; }
        }

        public int Length
        {
            get { return _array.Length; }
        }
        private T[] _array;

        public IndexableArray(T[] array)
        {
            _array = array;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _array.GetEnumerator();
        }
    }
}
