﻿using System.Drawing;
using System.Windows.Forms;
using Lab04.Controls;

namespace Lab04.States
{
    public class IdleState : IState
    {
        private IStateMachine _stateMachine;

        public IdleState(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void OnMouseDown(Point mousePoint, bool shift, bool ctrl) { }

        public void OnMouseUp(Point mousePoint, bool shift, bool ctrl) { }

        public void OnMouseDrag(Point mousePoint) { }

        public void OnKeyPressed(Keys key, Keys modifiers) { }

        public void OnDrawn(ICanvasRenderer renderer, GraphicalObject o) { }

        public void OnDrawn(ICanvasRenderer renderer) { }

        public void OnEnter() { }

        public void OnLeave() { }
    }
}
