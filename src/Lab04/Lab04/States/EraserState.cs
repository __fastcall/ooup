﻿using Lab04.Controls;
using Lab04.Utilities;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lab04.States
{
    public class EraserState : IState
    {
        public const int MouseTrailCapacity = 128;
        public const int MouseTrailChunkSize = 2;

        private IStateMachine _stateMachine;
        private Canvas _canvas;
        private List<Point> _mouseTrail = new List<Point>(MouseTrailCapacity);
        private int _mouseTrailCounter = 0;
        private bool _isMouseDown = false;

        public EraserState(IStateMachine stateMachine, Canvas canvas)
        {
            _stateMachine = stateMachine;
            _canvas = canvas;
        }

        public void OnMouseDown(Point mousePoint, bool shift, bool ctrl)
        {
            _mouseTrailCounter = 0;
            _isMouseDown = true;
        }

        public void OnMouseUp(Point mousePoint, bool shift, bool ctrl)
        {
            for (int i = 0; i < _mouseTrail.Count - 1; ++i)
            {
                GraphicalObject target = _canvas.Model.FindSelectedObject(_mouseTrail[i]);
                while (target != null)
                {
                    _canvas.Objects.Remove(target);
                    target = _canvas.Model.FindSelectedObject(_mouseTrail[i]);
                }
                Point point = new Point((_mouseTrail[i].X + _mouseTrail[i + 1].X) / 2, (_mouseTrail[i].Y + _mouseTrail[i + 1].Y) / 2);
                target = _canvas.Model.FindSelectedObject(point);
                while (target != null)
                {
                    _canvas.Objects.Remove(target);
                    target = _canvas.Model.FindSelectedObject(point);
                }
            }
            _isMouseDown = false;
            _mouseTrail.Clear();
            _canvas.Invalidate();
        }

        public void OnMouseDrag(Point mousePoint)
        {
            if ((_isMouseDown) && (_mouseTrail.Count < MouseTrailCapacity) && (++_mouseTrailCounter >= MouseTrailChunkSize))
            {
                _mouseTrailCounter = 0;
                _mouseTrail.Add(mousePoint);
                _canvas.Invalidate();
            }
        }

        public void OnKeyPressed(Keys key, Keys modifiers)
        {
            if (key == Keys.Escape)
            {
                _stateMachine.SwitchState(new IdleState(_stateMachine));
            }
        }

        public void OnDrawn(ICanvasRenderer renderer, GraphicalObject o) { }

        public void OnDrawn(ICanvasRenderer renderer)
        {
            for (int i = 0; i < _mouseTrail.Count - 1; ++i)
            {
                renderer.DrawLine(_mouseTrail[i], _mouseTrail[i + 1]);
            }
        }

        public void OnEnter() { }

        public void OnLeave()
        {
            _canvas.Invalidate();
        }
    }
}
