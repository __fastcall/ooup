﻿using System.Drawing;
using System.Windows.Forms;
using Lab04.Controls;

namespace Lab04.States
{
    public class AddShapeState : IState
    {
        private IStateMachine _stateMachine;
        private Canvas _canvas;
        private GraphicalObject _prototype;

        public AddShapeState(IStateMachine stateMachine, Canvas canvas, GraphicalObject prototype)
        {
            _stateMachine = stateMachine;
            _canvas = canvas;
            _prototype = prototype;
        }

        public void OnMouseDown(Point mousePoint, bool shift, bool ctrl)
        {
            GraphicalObject o = _prototype.Clone();
            o.Translate(mousePoint);
            _canvas.Objects.Add(o);
        }

        public void OnMouseUp(Point mousePoint, bool shift, bool ctrl) { }

        public void OnMouseDrag(Point mousePoint) { }

        public void OnKeyPressed(Keys key, Keys modifiers)
        {
            if (key == Keys.Escape)
            {
                _stateMachine.SwitchState(new IdleState(_stateMachine));
            }
        }

        public void OnDrawn(ICanvasRenderer renderer, GraphicalObject o) { }

        public void OnDrawn(ICanvasRenderer renderer) { }

        public void OnEnter() { }

        public void OnLeave() { }
    }
}
