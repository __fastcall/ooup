﻿namespace Lab04.States
{
    public interface IStateMachine
    {
        void SwitchState(IState newState);
    }
}
