﻿using Lab04.Controls;
using System.Drawing;
using System.Windows.Forms;

namespace Lab04.States
{
    public interface IState
    {
        void OnMouseDown(Point mousePoint, bool shift, bool ctrl);

        void OnMouseUp(Point mousePoint, bool shift, bool ctrl);

        void OnMouseDrag(Point mousePoint);

        void OnKeyPressed(Keys key, Keys modifiers);

        void OnDrawn(ICanvasRenderer renderer, GraphicalObject o);

        void OnDrawn(ICanvasRenderer renderer);

        void OnEnter();

        void OnLeave();
    }
}
