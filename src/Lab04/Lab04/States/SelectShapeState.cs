﻿using Lab04.Controls;
using Lab04.Drawing;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Lab04.States
{
    public class SelectShapeState : IState
    {
        private IStateMachine _stateMachine;
        private Canvas _canvas;
        private SelectablePoint _nearestHotPoint = null;

        public SelectShapeState(IStateMachine stateMachine, Canvas canvas)
        {
            _stateMachine = stateMachine;
            _canvas = canvas;
        }

        public void OnMouseDown(Point mousePoint, bool shift, bool ctrl)
        {
            if (_canvas.SelectedObjects.Count() == 1)
            {
                SelectablePoint nearestHotPoint = _canvas.Model.FindSelectedHotPoint(_canvas.SelectedObjects.First(), mousePoint);
                if (nearestHotPoint != null)
                {
                    _nearestHotPoint = nearestHotPoint;
                    return;
                }
            }
            if (!ctrl)
            {
                foreach (GraphicalObject o in _canvas.SelectedObjects)
                {
                    o.Selected = false;
                }
            }
            GraphicalObject nearestObject = _canvas.Model.FindSelectedObject(mousePoint);
            if (nearestObject != null)
            {
                nearestObject.Selected = !nearestObject.Selected;
            }
        }

        public void OnMouseUp(Point mousePoint, bool shift, bool ctrl)
        {
            _nearestHotPoint = null;
        }

        public void OnMouseDrag(Point mousePoint)
        {
            if (_nearestHotPoint != null)
            {
                _nearestHotPoint.X = mousePoint.X;
                _nearestHotPoint.Y = mousePoint.Y;
            }
        }

        public void OnKeyPressed(Keys key, Keys modifiers)
        {
            bool singleSelection = (_canvas.SelectedObjects.Count() == 1);
            switch (key)
            {
                case Keys.Up:
                    TranslateAllSelectedObjects(new Point(0, modifiers.HasFlag(Keys.Shift) ? -10 : -1));
                    break;
                case Keys.Down:
                    TranslateAllSelectedObjects(new Point(0, modifiers.HasFlag(Keys.Shift) ? 10 : 1));
                    break;
                case Keys.Left:
                    TranslateAllSelectedObjects(new Point(modifiers.HasFlag(Keys.Shift) ? -10 : -1, 0));
                    break;
                case Keys.Right:
                    TranslateAllSelectedObjects(new Point(modifiers.HasFlag(Keys.Shift) ? 10 : 1, 0));
                    break;
                case Keys.Add:
                    if (singleSelection)
                    {
                        if (modifiers.HasFlag(Keys.Shift))
                        {
                            _canvas.Model.MakeClosest(_canvas.SelectedObjects.First());
                        }
                        else
                        {
                            _canvas.Model.MakeCloser(_canvas.SelectedObjects.First());
                        }
                    }
                    break;
                case Keys.Subtract:
                    if (singleSelection)
                    {
                        if (modifiers.HasFlag(Keys.Shift))
                        {
                            _canvas.Model.MakeFurthest(_canvas.SelectedObjects.First());
                        }
                        else
                        {
                            _canvas.Model.MakeFurther(_canvas.SelectedObjects.First());
                        }
                    }
                    break;
                case Keys.G:
                    if (_canvas.SelectedObjects.Count() >= 2)
                    {
                        CompositeShape compositeShape = new CompositeShape(_canvas.SelectedObjects.ToArray());
                        foreach (GraphicalObject o in compositeShape.Objects)
                        {
                            _canvas.Objects.Remove(o);
                            o.Selected = false;
                        }
                        _canvas.Objects.Add(compositeShape);
                        compositeShape.Selected = true;
                    }
                    break;
                case Keys.U:
                    if ((singleSelection) && (_canvas.SelectedObjects.First() is CompositeShape))
                    {
                        CompositeShape compositeShape = (CompositeShape)_canvas.SelectedObjects.First();
                        _canvas.Objects.Remove(compositeShape);
                        compositeShape.Selected = false;
                        foreach (GraphicalObject o in compositeShape.Objects)
                        {
                            _canvas.Objects.Add(o);
                            o.Selected = true;
                        }
                        compositeShape.Dispose();
                    }
                    break;
                case Keys.Escape:
                    _stateMachine.SwitchState(new IdleState(_stateMachine));
                    break;
            }
        }

        public void OnDrawn(ICanvasRenderer renderer, GraphicalObject o)
        {
            if (o.Selected)
            {
                renderer.DrawRectangle(o.GetBoundingBox());
            }
        }

        public void OnDrawn(ICanvasRenderer renderer)
        {
            if (_canvas.SelectedObjects.Count() == 1)
            {
                foreach (SelectablePoint hotPoint in _canvas.SelectedObjects.First().HotPoints)
                {
                    Rectangle rectangle = new Rectangle(hotPoint.X - 4, hotPoint.Y - 4, 8, 8);
                    renderer.FillRectangle(rectangle);
                    renderer.DrawRectangle(rectangle);
                }
            }
        }

        public void OnEnter()
        {
            _canvas.Invalidate();
        }

        public void OnLeave()
        {
            foreach (GraphicalObject o in _canvas.SelectedObjects)
            {
                o.Selected = false;
            }
        }

        private void TranslateAllSelectedObjects(Point delta)
        {
            foreach (GraphicalObject o in _canvas.SelectedObjects)
            {
                o.Translate(delta);
            }
        }
    }
}
